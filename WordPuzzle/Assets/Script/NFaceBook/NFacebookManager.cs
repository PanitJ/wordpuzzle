﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Facebook.Unity;

public class NFacebookManager : MonoBehaviour {

	// Use this for initialization
	void Awake () {
		if (FB.IsInitialized) {
			FB.ActivateApp();
		} else {
			//Handle FB.Init
			FB.Init( () => {
				FB.ActivateApp();
			});
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
