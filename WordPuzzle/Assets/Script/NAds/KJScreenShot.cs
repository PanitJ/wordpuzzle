﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class KJScreenShot : MonoBehaviour {
#if UNITY_EDITOR
    string screenshotFilename;
	int superSize = 1;
	[SerializeField] string sceneName = "", lastSceneName = "";
	[SerializeField] string folderName = "ScreenShot";
	[SerializeField] bool isWide = true;
	double time, timeNow;
    bool isCountClear = false;
	
	void Start () {
		//PlayerPrefs.DeleteAll();
	    if (Camera.main.aspect > 1)
	    {
	        isWide = true;
	    }
	    else
	    {
	        isWide = false;
	    }
		TimeSpan t = (DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc));
		time = t.TotalSeconds;
	}
	
	public EditorWindow GetMainGameView()
	{
		System.Type T = System.Type.GetType("UnityEditor.GameView,UnityEditor");
		System.Reflection.MethodInfo GetMainGameView = T.GetMethod("GetMainGameView",System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Static);
		System.Object Res = GetMainGameView.Invoke(null,null);
		return (EditorWindow)Res;
	}
	
	public Vector2 GetMainGameViewSize()
	{
		System.Type T = System.Type.GetType("UnityEditor.GameView,UnityEditor");
		System.Reflection.MethodInfo GetSizeOfMainGameView = T.GetMethod("GetSizeOfMainGameView",System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Static);
		System.Object Res = GetSizeOfMainGameView.Invoke(null,null);
		return (Vector2)Res;
	}

	public void CaptureScreenshot () {
		int x, y;
		print("CaptureScreenshot");
//		iPhone 3+4 (3.5 Inch) 640 x 960.
//		iPhone 5 (4 Inch) 640 x 1136.
//		iPhone 6 (4.7 Inch) 750 x 1334.
//		iPhone 6 Plus (5.5 Inch) 1242 x 2208. ...
//		iPad (Air and Mini Retina) 1536 x 2048.
//		iPad Pro. 2048 x 2732.

		//3.5-Inch Retina Display 960 * 640
		if (!isWide) {
			x = 640;
			y = 960;
		} else {
			x = 960;
			y = 640;
		}
		if (GetMainGameViewSize().x == x && GetMainGameViewSize().y == y) {
			superSize = 1;
			Screenshot();
			//KJTime.Add(ScreenshotWithSize, 0.1f, 1);
		}
			
		//iPhone 5 (4 Inch) Display 1136 x 640
		if (!isWide) {
			x = 640;
			y = 1136;
		} else {
			x = 1136;
			y = 640;
		}
		if (GetMainGameViewSize().x == x && GetMainGameViewSize().y == y) {
			superSize = 1;
			Screenshot();
		}
			
		//iPad (Air and Mini Retina) 2048 x 1536
		if (!isWide) {
			x = 1536;
			y = 2048;
		} else {
			x = 2048;
			y = 1536;
		}
		if (GetMainGameViewSize().x == x && GetMainGameViewSize().y == y) {
			superSize = 1;
			Screenshot();
		}

        //iPad (Air and Mini Retina) 2732 x 2048
        if (!isWide)
        {
            x = 2048;
            y = 2732;
        }
        else
        {
            x = 2732;
            y = 2048;
        }
        if (GetMainGameViewSize().x == x && GetMainGameViewSize().y == y)
        {
            superSize = 1;
            Screenshot();
        }

        //iPhone 6 (4.7 Inch) 750 x 1334.
        if (!isWide) {
			x = 750;
			y = 1334;
		} else {
			x = 1334;
			y = 750;
		}
		if (GetMainGameViewSize().x == x && GetMainGameViewSize().y == y) {
			superSize = 1;
			Screenshot();
		}

		//iPhone 6 Plus (5.5 Inch)  2208 x 1242 
		if (!isWide) {
			x = 1242;
			y = 2208;
		} else {
			x = 2208;
			y = 1242;
		}
		if (GetMainGameViewSize().x == x && GetMainGameViewSize().y == y) {
			superSize = 1;
			Screenshot();
		}

		//OSX 2880 x 1800
		if (!isWide) {
			x = 1800;
			y = 2880;
		} else {
			x = 2880;
			y = 1800;
		}
		if (GetMainGameViewSize().x == x && GetMainGameViewSize().y == y) {
			superSize = 1;
			Screenshot();
		}

		if (!isWide) {
			x = 1080;
			y = 1920;
		} else {
			x = 1920;
			y = 1080;
		}
		// 1920 x 1080
		if (GetMainGameViewSize().x == x && GetMainGameViewSize().y == y) {
			superSize = 1;
			Screenshot();
		}


	}

	void CleanDicCount () {
		count.Clear();
	}

	//int count = 1;
	Dictionary<string, int> count = new Dictionary<string, int>();
	string lastKeyDic = "";
    string sizeName;
    void Screenshot () {

		//KJInput.Lock ();
		//KJTime.PauseAll ();
		
		string keyDic;
		Vector2 screenSize;
		screenSize = new Vector2(GetMainGameViewSize().x * superSize, GetMainGameViewSize().y * superSize);
		sizeName = screenSize.x + " x " + screenSize.y;
		keyDic = screenSize.x +"x"+ screenSize.y;

		sceneName = Application.loadedLevelName;
		if (sceneName != lastSceneName) {
			if (isCountClear) count.Clear();
		}

		if (!count.ContainsKey(keyDic)) {
			count.Add(keyDic, 1);
		}

		lastKeyDic = keyDic;
		lastSceneName = Application.loadedLevelName;
		TimeSpan t = (DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc));
		timeNow = t.TotalSeconds;
		screenshotFilename = sizeName + "_" + count[keyDic] + ".png";
        //Application.CaptureScreenshot(PathToImageByScene, superSize);
        ScreenCapture.CaptureScreenshot(PathToImage, superSize);
        count[keyDic]++;
		Debug.Log(PathToImage);

	}

	bool isPause = false;
	void PauseTime () {
		if (isPause) {
			Debug.Log("Game Pause");
			isPause = false;
			//KJInput.Lock ();
			//KJTime.PauseAll ();
		} else {
			isPause = true;
			//KJInput.Unlock ();
			//KJTime.ResumeAll ();
		}
	}

	void Update () {

		if(Input.GetKeyDown(KeyCode.F4))
		{
			PauseTime();
		}

		if(Input.GetKeyDown(KeyCode.F5))
		{
			//KJInput.Unlock ();
			//KJTime.ResumeAll ();
			CaptureScreenshot();
		}

		if(Input.GetKeyDown(KeyCode.F7))
		{
			CleanDicCount();
		}

		if(Input.GetKeyDown(KeyCode.F6))
		{
			//KJInput.Unlock ();
			//KJTime.ResumeAll ();
		}
	}

    string PathToImage
    {
        get
        {
            return Application.persistentDataPath + "/" + folderName + "/" + System.IO.Directory.CreateDirectory(Application.persistentDataPath + "/" + folderName + "/"  + sizeName +"_"+ time) + "/" + screenshotFilename;
        }
    }

    string PathToImageByScene {
		get {
			return Application.persistentDataPath +"/"+ folderName +"/" + System.IO.Directory.CreateDirectory(Application.persistentDataPath +"/"+ folderName +"/"+ sceneName + time)+ "/" + screenshotFilename;
		}
	}
#endif

}

