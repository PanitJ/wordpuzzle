﻿using UnityEngine;
using UnityEngine.UI;
using K2.Popup;


public class HelpPopup : PopupContent
{
    [SerializeField] GameObject[] _helpText;
    [SerializeField] Image[] _helpIndex;
    [SerializeField] Sprite _indexOfInterest;
    [SerializeField] Sprite _indexOutOfInterest;

    private int _currentIndex;
    // Use this for initialization
    public HelpPopup CloneAndDeploy(int index = 0)
    {
        var clone = Instantiate(this);
        clone.Deploy(index);
        return clone;
    }


    public void Deploy(int index)
    {
        _currentIndex = index;
        Refresh();
    }

    void Refresh ()
    {
        ShowPage(_currentIndex);
    }

    void ShowPage(int index)
    {
        for(int i = 0; i < _helpText.Length; i++)
        {
            bool isIndexOfinterest = i == index;
            _helpText[i].SetActive(isIndexOfinterest);
            SetDot(i, isIndexOfinterest);
        }
    }

    void SetDot (int index, bool isInterest)
    {
        if (isInterest)
            _helpIndex[index].sprite = _indexOfInterest;
        else
            _helpIndex[index].sprite = _indexOutOfInterest;
    }


    public void Next ()
    {
        SoundManager.instance.PlayButtonSound();
        _currentIndex++;
        if (_currentIndex >= _helpText.Length)
            _currentIndex = 0;
        Refresh();
    }

    public void Previous ()
    {
        SoundManager.instance.PlayButtonSound();
        _currentIndex--;
        if (_currentIndex < 0)
            _currentIndex = _helpText.Length - 1;
        Refresh();
    }

    public void Close()
    {
        SoundManager.instance.PlayButtonSound();
        PopupSystem.Instance.CloseLayer();
    }
}
