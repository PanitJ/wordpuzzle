﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ModeSceneController : MonoBehaviour {

    [SerializeField] private ModeGroup _modeGroupPrefab;
    [SerializeField] private Mode _modePrefab;
    [SerializeField] private HeaderBarController _headerBarController;
    [SerializeField] private GameObject _parent;
    private const float GAP_Y = -300;
    private const float GAP_SUB_Y = 120f;
    private const float beginOffset = 350f;

    Vector3 currentPointPosition = new Vector3(0,-beginOffset, 0);


    void Start ()
    {
        int k = 0;
        for (int i = 0; i < LevelManager.instance.ThemeCount; i++)
        {
            LevelData[] levelDatas = new LevelData[LevelManager.instance.CountForTheme(i)];
            var beginIndex = k;
            for (int j = 0; j < LevelManager.instance.CountForTheme(i); j++)
            {
                levelDatas[j] = LevelManager.instance.GetMainLevel(k);
                Debug.Log("is cali clear " + levelDatas[j].HasClear + "  name " + levelDatas[j].name);
                k++;
            }
            var parent = CreateHead(levelDatas, currentPointPosition,beginIndex);
            currentPointPosition.y += -parent.Height - GAP_SUB_Y;

        }


        _parent.GetComponent<RectTransform>().sizeDelta = new Vector2(1000f, EstimateContentSize());
        _headerBarController.Calibrate(BackToSplash, false);
        SoundManager.instance.PlayMenuMusic();

    }

    private  ModeGroup CreateHead (LevelData[] leveldatas, Vector3 positioVector3, int beginIndex)
    {
        var newMode = Instantiate<ModeGroup>(_modeGroupPrefab, _parent.transform);
        newMode.Calibrate(leveldatas, beginIndex);

        newMode.transform.localPosition = positioVector3 - new Vector3(0,(newMode.Height - 340f)/2f,0);
            //-  new Vector3(0,newMode.Height *2,0);

        return newMode;

    }

    void BackToSplash()
    {
        SceneManager.LoadScene("Splash");
    }

    float EstimateContentSize ()
    {
        var gapCount = LevelManager.instance.ThemeCount- 1;
        return (ModeGroup.Base_Height * LevelManager.instance.ThemeCount) + (GAP_SUB_Y * gapCount) + (ModeGroup.Content_Height * LevelManager.instance.allLevels.Count) + (beginOffset/2);
    }

}
 