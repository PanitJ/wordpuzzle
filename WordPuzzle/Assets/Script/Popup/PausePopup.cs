﻿using K2.Popup;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PausePopup : PopupContent {
    public void Quit()
    {
        SceneManager.LoadScene("Menu");
    }
}
