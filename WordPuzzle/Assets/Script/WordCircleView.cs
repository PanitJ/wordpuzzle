﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WordCircleView : MonoBehaviour
{
    [SerializeField] public Text _label;
    [SerializeField] public Image _labelFrame;
    [SerializeField] private float _nodeRadius = 50f;
    [SerializeField] private Line _prefabLine;
    [SerializeField] private Node _prefabNode;
    private float _radius = 0.64f;
    [SerializeField] private float _localRadius = 250.0f;

    [SerializeField] private LineRender _lineRender;
    [SerializeField] private TextPreview _textPreview;
    private char[] _alphabets;

    private readonly Color[] _colorList = new Color[]
        {Color.red, Color.yellow, Color.green, Color.blue, Color.cyan, Color.white, Color.magenta};

    private int _currentLineIndex = 0;
    private Node _currentNode;
    public GameController gameController;
    private bool _isFirstDrag = true;

    private List<Line> _lines = new List<Line>();
    private int _maximumLine = 0;
    private List<Node> _nodes = new List<Node>();
    private string _selectedWord = null;
    private int _nodeConnectedCount = 0;
    public List<Vector3> letterPositions = new List<Vector3>();


    const float REVESE_ANGLE = 180f;
    const float REVESE_ANGLE_RANGE = 15f;

    public event Action OnNodeConnected;
    
    public bool IsFirstDrag
    {
        get { return _isFirstDrag; }

        set { _isFirstDrag = value; }
    }

    public void Calibrate(char[] alphabets)
    {
        _textPreview.Reset();
        _alphabets = alphabets;
        InitNode(alphabets.Length);
        _lineRender.Calibrate(_radius,letterPositions,this.transform.position);
        _lineRender.OnRelease += CheckAnswer;
        _textPreview.SetLetter(alphabets);
    }

    private void InitNode(int nodeNumber)
    {
        for (int i = 0; i < nodeNumber; i++)
        {
            var angle = i * Mathf.PI * 2 / nodeNumber;
            var pos = new Vector3(Mathf.Sin(angle), Mathf.Cos(angle), 0) * (_localRadius - _nodeRadius);
            var newNode = Instantiate<Node>(_prefabNode, transform);
            newNode.transform.localPosition = pos;
            newNode.Calibrate(_nodes, _colorList[i % _colorList.Length], _alphabets[i % _alphabets.Length]);
            newNode.OnBeginDragUI += _lineRender.Dragging;
            newNode.OnEndDragUI += _lineRender.Release;
            _nodes.Add(newNode);
            letterPositions.Add(newNode.transform.position);

        }
    }

    private void CheckAnswer(string answer)
    {
        InputController.Lock();
        var lineColor = gameController.CheckAnswer(answer);
        _textPreview.SetColor(lineColor);
        if (lineColor == Color.red)
            ShakeNodes();
    }

    void ShakeNodes()
    {
        foreach (var node in _nodes)
        {
            iTween.ShakePosition(node.gameObject,new Vector3(0.1f,0.1f,0.1f),0.5f);
        }
    }

    public void Reset()
    {
        InputController.Unlock();
        _textPreview.FadeOut();
    }

    public void Shuffle ()
    {
        List<int> randomed = new List<int>();
        List<Vector3> tempPosition = new List<Vector3>();
        int i = 0;
        foreach (var node in _nodes)
        {
            int randomIndex = RandomIndex(randomed, _nodes.Count);
            randomed.Add(randomIndex);
            var destination = _nodes[randomIndex].gameObject.transform.localPosition;
            iTween.MoveTo(node.gameObject, iTween.Hash("position", destination, "time", GameController.SHUFFLE_TIME, "isLocal", true));
            tempPosition.Add(letterPositions[randomIndex]);
            i++;
        }
        letterPositions.Clear();
       for(int j = 0; j < _nodes.Count; j++)
        {
            letterPositions.Add(tempPosition[j]);
            Debug.Log("nodE " + _nodes[j].Letter + " pos  " + letterPositions[j]);
        }

       ///A B C
        _lineRender.letterPositions = letterPositions;
    }


    private int RandomIndex(List<int> randomed, int max)
    {
        var result = 0;
        do
        {
            result = UnityEngine.Random.Range(0, max);
        }
        while (randomed.Contains(result));
        return result ;
    }

}