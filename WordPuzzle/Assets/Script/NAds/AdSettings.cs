using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AdSettings : MonoBehaviour
{

    private static AdSettings _Singleton;
    private static GameObject parentObject;
    public static AdSettings Instance
    {
        get
        {
            if (!_Singleton)
            {
                
                parentObject = new GameObject("AdSettings");
                DontDestroyOnLoad(parentObject);
                _Singleton = parentObject.AddComponent<AdSettings>();
                //_Singleton.Initialize();
            }
            return _Singleton;
        }
    }

//#if UNITY_ANDROID
//    public static string adMobInterID = "ca-app-pub-4783717320434871/1336107149";
//#elif UNITY_IPHONE
//	public static string adMobInterID = "ca-app-pub-6802294304749500/5291154678";
//#endif

//#if UNITY_ANDROID
//    public static string unityID = "1418409";
//#elif UNITY_IPHONE
//    public static string unityID = "1436153";
//#endif

//#if UNITY_ANDROID
//    public static string adMobBannerID = "ca-app-pub-4783717320434871/6701848346";
//#elif UNITY_IPHONE
//    public static string adMobBannerID = "ca-app-pub-6802294304749500/3814421477";
//#endif

//#if UNITY_ANDROID
//    public static string adMobRewardID = "ca-app-pub-4783717320434871/8178581543";
//#elif UNITY_IPHONE
//    public static string adMobRewardID = "ca-app-pub-4783717320434871/1945198347";
//#endif




}
