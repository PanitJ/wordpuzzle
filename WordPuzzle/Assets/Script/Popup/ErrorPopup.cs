﻿using K2.Popup;
using UnityEngine;
using UnityEngine.UI;
using System;

public class ErrorPopup : PopupContent
{
    [SerializeField] private Text bodyText;
    [SerializeField] private GameObject buttonOption;
    [SerializeField] private Text optionButtonText;
    public event Action OnOption;


    public void Deploy(string text, string actionName, Action action)
    { 
        Deploy(text);
        buttonOption.gameObject.SetActive(true);
        optionButtonText.text = actionName;
        OnOption = action;
    }

    public void Deploy(string text)
    {
        
        bodyText.text = text;
    }

    public void Close()
    {
        SoundManager.instance.PlayButtonSound();
        PopupSystem.Instance.CloseLayer();
    }

    public void OnOptionClick ()
    {
        if (OnOption != null)
            OnOption();
    }
}
