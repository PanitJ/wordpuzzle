﻿using UnityEngine;

namespace K2.Unity
{
    [CreateAssetMenu(menuName = "K2/Flow Controller Config")]
    public class SceneController : ScriptableObject
    {
        [SerializeField] private SceneTransitioner _sceneTransitioner;

        public void FadeToScene(string sceneName)
        {
            var transitioner = Instantiate(_sceneTransitioner);
            transitioner.Initialize(sceneName);
        }
    }
}