﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace K2
{
    public class Time : MonoBehaviour
    {
        public delegate void TimeDelegate();

        private static GameObject _parent;
        private static Time _singleton;
        private List<TimeObject> _addList;

        private Timeline DEFAULT_GAME_TIMELINE, DEFAULT_SYSTEM_TIMELINE;
        private bool _hasBeenInitialized = false;
        private List<TimeDelegate> _removalList;
        private Dictionary<string, Timeline> _timelineDic;

        private Dictionary<TimeDelegate, TimeObject> _timeObjectDic;

        public static Time Singleton
        {
            get
            {
                if (_singleton != null)
                    return _singleton;

                CreateSingleton();
                return _singleton;
            }
        }

        private static void CreateSingleton()
        {
            _parent = new GameObject("K2: Time");
            DontDestroyOnLoad(_parent);
            _singleton = _parent.AddComponent<Time>();
            _singleton.Initialize();
        }

        public static Timeline GameTimeline
        {
            get { return Singleton.DEFAULT_GAME_TIMELINE; }
        }

        public static Timeline SystemTimeline
        {
            get { return Singleton.DEFAULT_SYSTEM_TIMELINE; }
        }

        private void Initialize()
        {
            if (_hasBeenInitialized)
                return;

            _hasBeenInitialized = true;
            _timeObjectDic = new Dictionary<TimeDelegate, TimeObject>();
            _timelineDic = new Dictionary<string, Timeline>();
            _addList = new List<TimeObject>();
            _removalList = new List<TimeDelegate>();

            DEFAULT_GAME_TIMELINE = CreateTimeline("DEFAULT_GAME_TIMELINE");
            DEFAULT_SYSTEM_TIMELINE = CreateTimeline("DEFAULT_SYSTEM_TIMELINE");
        }

        public static Timeline CreateTimeline(string name, float speed = 1f)
        {
            if (Singleton._timelineDic.ContainsKey(name))
                return Singleton._timelineDic[name];

            var timeline = new Timeline(name, speed);
            Singleton._timelineDic.Add(name, timeline);
            return Singleton._timelineDic[name];
        }

        public static Timeline GetTimeline(string name)
        {
            if (!Singleton._timelineDic.ContainsKey(name))
                CreateTimeline(name);
            return Singleton._timelineDic[name];
        }

        private void Update()
        {
            UpdateAllTimers();
            RemoveUnusedTimers();
            AddPendingTimers();
        }

        private void AddPendingTimers()
        {
            foreach (var timeObject in _addList) Singleton._timeObjectDic.Add(timeObject.TimeDelegate, timeObject);
            _addList.Clear();
        }

        private void RemoveUnusedTimers()
        {
            foreach (var timeDelegate in _removalList)
                Singleton._timeObjectDic.Remove(timeDelegate);

            _removalList.Clear();
        }

        private static void UpdateAllTimers()
        {
            foreach (var timeObject in Singleton._timeObjectDic.Values)
                timeObject.Update(UnityEngine.Time.deltaTime);
        }

        public static TimeObject Add(TimeDelegate timeDelegate, float delayInSeconds = 0f, int repeatCount = 0,
            bool inGameTime = true, object originObject = null)
        {
            var timeline = inGameTime ? Singleton.DEFAULT_GAME_TIMELINE : Singleton.DEFAULT_SYSTEM_TIMELINE;
            return Add(timeDelegate, timeline, delayInSeconds, repeatCount, originObject);
        }

        public static TimeObject Add(TimeDelegate timeDelegate, string timelineTag, float delayInSeconds = 0f,
            int repeatCount = 0, object originObject = null)
        {
            var timeline = GetTimeline(timelineTag);
            return Add(timeDelegate, timeline, delayInSeconds, repeatCount, originObject);
        }

        public static TimeObject Add(TimeDelegate timeDelegate, Timeline timeline, float delayInSeconds = 0f,
            int repeatCount = 0, object originObject = null)
        {
            var timeObject = Singleton._timeObjectDic.ContainsKey(timeDelegate) ? Singleton._timeObjectDic[timeDelegate] : CreateNewTimeObject();
            timeObject.Initialize(timeline, timeDelegate, originObject, delayInSeconds, repeatCount);
            return timeObject;
        }

        private static TimeObject CreateNewTimeObject()
        {
            var timeObject = new TimeObject();
            Singleton._addList.Add(timeObject);
            return timeObject;
        }

        public static void Remove(TimeDelegate timeDelegate)
        {
            if (Singleton._timeObjectDic.ContainsKey(timeDelegate))
            {
                var timeObject = Singleton._timeObjectDic[timeDelegate];
                if (timeObject.IsActive)
                {
                    timeObject.IsActive = false;
                    Singleton._removalList.Add(timeDelegate);
                }
            }

            // Prevent a timer from being added after this removal.
            for (int i = Singleton._addList.Count - 1, m = -1; i > m; i--)
                if (Singleton._addList[i].TimeDelegate == timeDelegate) Singleton._addList.RemoveAt(i);
        }

        public static void RemoveFromObject(object originObject)
        {
            if (originObject == null)
                return;

            foreach (var timeObject in Singleton._timeObjectDic.Values)
                if (timeObject.HasOriginObject(originObject)) timeObject.Remove();
        }

        public static void Remove(Timeline timeline)
        {
            foreach (var timeObject in Singleton._timeObjectDic.Values)
                if (timeObject.IsInTimeline(timeline)) timeObject.Remove();
        }

        public static void RemoveAll()
        {
            Remove(GameTimeline);
        }

        public static void Pause(Timeline timeline)
        {
            timeline.IsPaused = true;
        }

        public static void Pause(string timelineTag)
        {
            Pause(GetTimeline(timelineTag));
        }

        public static void Pause()
        {
            Pause(GameTimeline);
        }

        public static void Resume(Timeline timeline)
        {
            timeline.IsPaused = false;
        }

        public static void Resume(string timelineTag)
        {
            Resume(GetTimeline(timelineTag));
        }

        public static void Resume()
        {
            Resume(GameTimeline);
        }

        public static List<TimeObject> GetTimerList(Timeline timeline = null)
        {
            var timerList = Singleton._timeObjectDic.Values.Where(timeObject => timeObject.Timeline == timeline || timeline == null).ToList();

            foreach (var timeDelegate in Singleton._removalList)
                for (int i = timerList.Count - 1, m = -1; i > m; i++)
                    if (timerList[i].TimeDelegate == timeDelegate)
                        timerList.RemoveAt(i);

            foreach (var timeObject in Singleton._addList)
                if (timeObject.Timeline == timeline || timeline == null)
                    if (!timerList.Contains(timeObject)) timerList.Add(timeObject);

            return timerList;
        }
    }

    public class Timeline
    {
        public bool IsPaused;
        public string Name;
        public float Speed;

        public Timeline(string name, float speed)
        {
            Name = name;
            Speed = speed;
            IsPaused = false;
        }
    }

    public class TimeObject
    {
        private bool _isFrameUpdate = false;
        private float _maxGapInSeconds = 1.0f, _currentGapInSeconds = 0.0f;
        private int _maxRepeatCount = 0, _currentRepeatCount = 0;
        private object _originObject;

        public Time.TimeDelegate TimeDelegate;
        public Timeline Timeline;

        public TimeObject()
        {
            IsActive = false;
        }

        public bool IsActive { get; set; }

        public float SecondsUntilNextTick
        {
            get { return _isFrameUpdate ? 0 : _maxGapInSeconds - _currentGapInSeconds; }
        }

        public float Progress
        {
            get { return _isFrameUpdate ? 0 : _currentGapInSeconds / _maxGapInSeconds; }
        }

        public void Initialize(Timeline timeline, Time.TimeDelegate timeDelegate, object originObject,
            float gapInSeconds, int repeatCount)
        {
            Timeline = timeline;
            TimeDelegate = timeDelegate;
            _originObject = originObject;
            _maxGapInSeconds = gapInSeconds;
            _maxRepeatCount = repeatCount;

            _isFrameUpdate = _maxGapInSeconds == 0.0f;
            _currentRepeatCount = 0;
            _currentGapInSeconds = 0.0f;

            IsActive = true;
        }

        public void Update(float deltaTime)
        {
            if (Timeline.IsPaused || !IsActive)
                return;

            if (_isFrameUpdate)
                TimeDelegate();
            else
                RunActualTimer(deltaTime);
        }

        private void RunActualTimer(float deltaTime)
        {
            _currentGapInSeconds += Timeline.Speed * deltaTime;

            if (!(_currentGapInSeconds >= _maxGapInSeconds))
                return;

            TimeDelegate();
            _currentGapInSeconds = 0.0f;
            _currentRepeatCount++;

            if (_currentRepeatCount == _maxRepeatCount)
                Remove();
        }

        public bool IsInTimeline(Timeline compareTimeline)
        {
            return Timeline == compareTimeline;
        }

        public bool HasOriginObject(object targetObject)
        {
            return _originObject == targetObject;
        }

        public void Remove()
        {
            Time.Remove(TimeDelegate);
        }

        public override string ToString()
        {
            return string.Format("[TimeObject: IsActive={0} GapCount={1} Progress={2}]", IsActive, _maxGapInSeconds,
                Progress);
        }
    }
}