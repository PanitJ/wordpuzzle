﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NIsRemoveAds : MonoBehaviour {

	public string key = "isRemoveAds";

	// Use this for initialization
	void Start () {
        
	    if ((PlayerPrefsX.GetBool(key)))
	    {
	        gameObject.SetActive(false);
	    }
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
