﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class NAdManager : MonoBehaviour
{

	public int _countForAds = 2;
	int countForAds = 0;
	DateTime nextAdsTime = new DateTime ();
	int delayTimeSec = 5;
	bool isRequestBanner = false;
	bool isLock = false;

	[Header("Andriod")]
	public string adMobInterAndriodID = "";
	public string unityAndriodID = "";
	public string adMobBannerAndriodID = "";
	[Header("IOS")]
	public string adMobInterIOSID = "";
	public string unityIOSID = "";
	public string adMobBannerIOSID = "";

	public Action callActionGG = null;

	public static NAdManager Instance { set; get; }

	void Awake ()
	{
		if (Instance == null) {
			Instance = this;
			DontDestroyOnLoad (gameObject);
			ResetCountAds ();
			Instance.Init ();
		} else {
			Destroy (gameObject);
		}

		//RequestAdmobNativeExpressAdView();
		//DeployWaiting();
	}

	public void ResetCountAds ()
	{
		countForAds = _countForAds - 1;
	}

	public void ShowAds (bool isShow = false)
	{
		if (PlayerPrefsX.GetBool ("isRemoveAds"))
			return;
		print ("ShowAds");
		if ((--countForAds < 0 ) || isShow) {
			//NGoogleAds.Instance.ShowInterstitial ();
			AdManagerUnity.Instance.ShowAdNormal();
		}
	}


	public void NShowAds () {
		ShowAds();
	}

	public void DisBannerAdmob ()
	{
		if (NGoogleAds.Instance.bannerView != null)
			NGoogleAds.Instance.bannerView.Destroy ();
	}

	public void ShowAdmodInter ()
	{
		if (PlayerPrefsX.GetBool ("isRemoveAds"))
			return;
		NGoogleAds.Instance.ShowInterstitial ();
	}

	public void ShowAdmobBanner ()
	{
		if (PlayerPrefsX.GetBool ("isRemoveAds"))
			return;
		isRequestBanner = true;
		NGoogleAds.Instance.RequestBanner ();
	}

	public void ShowAdRewardUnity (Action callBack = null)
	{
		AdManagerUnity.Instance.ShowAdReward ("", callBack);
	}

	public void ShowAdTestRewardUnity (Action callBack = null)
	{
		AdManagerUnity.Instance.ShowAdReward ("", callBack);
	}

	public void ShowAdRewardGG (Action callBack = null)
	{
		callActionGG = callBack;
		NGoogleAds.Instance.ShowRewardBasedVideo ();
	}

	public void RequestAdmobNativeExpressAdView ()
	{
		if (PlayerPrefsX.GetBool ("isRemoveAds"))
			return;
		NGoogleAds.Instance.RequestNativeExpressAdView ();
	}

	public void ShowNativeAdmob () {
		if (PlayerPrefsX.GetBool ("isRemoveAds"))
			return;
		NGoogleAds.Instance.ShowNative();
	}

	public void HideAdmobNative () {
		NGoogleAds.Instance.HideNative();
	}

	private bool isUnityAdsReward = false;

	public void SwitchAdsReward (Action callBack = null)
	{
		if (isUnityAdsReward) {
			ShowAdRewardUnity (callBack);
			isUnityAdsReward = false;
		} else {
			ShowAdRewardGG (callBack);
			isUnityAdsReward = true;
		}
	}

	public void Init ()
	{
		NGoogleAds.Instance.Initialize ();
		AdManagerUnity.Instance.Initialize ();
		ShowAdmobBanner();
		//RequestAdmobNativeExpressAdView();
	}

}
