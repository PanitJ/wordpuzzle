﻿using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Mode : MonoBehaviour
{
    [SerializeField] private Text text;
    [SerializeField] private Text status;
    private TextAsset _textAsset;
    [SerializeField] private Image _badge;

    [SerializeField] Sprite _solve;
    [SerializeField] Sprite _unSolve;
    [SerializeField] Sprite _lock;
    [SerializeField] Sprite _unlock;

    [SerializeField] private Button _button;
    public int MainLevel = 0;
    public LevelData levelData;

    public void Calibrate(LevelData levelData, int index)
    {
        this.levelData = levelData;
        name = levelData.name;
        text.text = name;

        //  icon.sprite = levelData.iconSprite;
        _textAsset = levelData.Asset;
        _button.interactable = true;

        Debug.Log("level dat " +levelData.mainLevel + "   " + LevelManager.instance.MaxProgress + " isclear " + levelData.HasClear);

        if (levelData.HasClear)
        {
            _badge.sprite = _unlock;
            status.text = "Solved";
        ///    _status.sprite = _solve;
        }
        else if (levelData.mainLevel == LevelManager.instance.MaxProgress)
        {
            //      _status.sprite = _unSolve;
            status.text = "Current";

            _badge.sprite = _unlock;
        }
        else
        {
            //    _status.sprite = _unSolve;
            status.text = "Unsolved";
            _badge.sprite = _lock;
           _button.interactable = false;
        }

        //if (levelData.index > LevelManager.instance.MaxProgress)
        //{
        //    _badge.sprite = _lock;
        //    GetComponent<Button>().interactable = false;
        //}
    }

    public void GotoLevel()
    {
        SoundManager.instance.PlayButtonSound();
        // icon.sprite = levelData.iconSpriteActive;

        //  if (Profile.Instance.Asset != _textAsset)
        if (Profile.Instance.CurrentLevelData != levelData)
        {
            Profile.Instance.CurrentPlayedWords.Clear();
            //  Profile.Instance.Asset = _textAsset;
            Profile.Instance.CurrentLevelData = levelData;
            // Profile.Instance.CurrentMainLevel = MainLevel;
            Profile.Instance.CurrentMainLevel = levelData.mainLevel;
        }
            Debug.Log("level " + levelData.name);

            SceneManager.LoadScene("Menu");
        FireBaseManager.instance.LogModeSelect(levelData.mainLevel);
    }

}
