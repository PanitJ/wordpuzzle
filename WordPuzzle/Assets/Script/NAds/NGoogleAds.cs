using System;
using UnityEngine;
using GoogleMobileAds;
using GoogleMobileAds.Api;

// Example script showing how to invoke the Google Mobile Ads Unity plugin.
public class NGoogleAds : MonoBehaviour
{
    public BannerView bannerView;
    private InterstitialAd interstitial;
    private NativeExpressAdView nativeExpressAdView;
    private RewardBasedVideoAd rewardBasedVideo;
    private float deltaTime = 0.0f;
    private static string outputMessage = string.Empty;
    public static bool isStartNative = true;

    bool disableBanner = false;

    public static string OutputMessage
    {
        set { outputMessage = value; }
    }

    private static GameObject parentObject;

    private static NGoogleAds _Singleton;

    public static NGoogleAds Instance
    {
        get
        {
            if (!_Singleton)
            {
                parentObject = new GameObject("NGoogleAds");
                DontDestroyOnLoad(parentObject);
                _Singleton = parentObject.AddComponent<NGoogleAds>();
                //_Singleton.Init();
            }
            return _Singleton;
        }
    }

    void Start ()
    {
        //Instance.Init();
        //RequestBanner();
    }

    public void Initialize ()
    {
//        rewardBasedVideo = RewardBasedVideoAd.Instance;
//        // RewardBasedVideoAd is a singleton, so handlers should only be registered once.
//        rewardBasedVideo.OnAdLoaded += HandleRewardBasedVideoLoaded;
//        rewardBasedVideo.OnAdFailedToLoad += HandleRewardBasedVideoFailedToLoad;
//        rewardBasedVideo.OnAdOpening += HandleRewardBasedVideoOpened;
//        rewardBasedVideo.OnAdStarted += HandleRewardBasedVideoStarted;
//        rewardBasedVideo.OnAdRewarded += HandleRewardBasedVideoRewarded;
//        rewardBasedVideo.OnAdClosed += HandleRewardBasedVideoClosed;
//        rewardBasedVideo.OnAdLeavingApplication += HandleRewardBasedVideoLeftApplication;
        RequestInterstitial();
        //RequestNativeExpressAdView();
    }

    public void Update()
    {
        // Calculate simple moving average for time to render screen. 0.1 factor used as smoothing
        // value.
        //deltaTime += (Time.deltaTime - deltaTime) * 0.1f;
    }

    // Returns an ad request with custom ad targeting.
    private AdRequest CreateAdRequest()
    {
        return new AdRequest.Builder()
            //.AddTestDevice(AdRequest.TestDeviceSimulator)
            //.AddTestDevice("654D577E376E5B4964FC20FBBC62C9BF")
            //.AddKeyword("game")
            //.SetGender(Gender.Male)
            //.SetBirthday(new DateTime(1985, 1, 1))
            //.TagForChildDirectedTreatment(false)
            //.AddExtra("color_bg", "9B30FF")
                .Build();
    }

    int CalculateBannerHeight() {
        if (Screen.height <= 400*Mathf.RoundToInt(Screen.dpi/160)) {
            return 32*Mathf.RoundToInt(Screen.dpi/160);
        } else if (Screen.height <= 720*Mathf.RoundToInt(Screen.dpi/160)) {
            return 50*Mathf.RoundToInt(Screen.dpi/160);
        } else {
            return 90*Mathf.RoundToInt(Screen.dpi/160);
        }
    }

    public void RequestBanner()
    {

        // These ad units are configured to always serve test ads.
        if (disableBanner) return;
        #if UNITY_ANDROID
		string adUnitId = NAdManager.Instance.adMobBannerAndriodID;
        #elif UNITY_IPHONE
		string adUnitId = NAdManager.Instance.adMobBannerIOSID;
        #else
		string adUnitId = "";
		#endif
        int widthPixels = AdSize.SmartBanner.Width;
        int heightPixels = AdSize.SmartBanner.Height;

        // Create a 320x50 banner at the top of the screen.
		bannerView = new BannerView(adUnitId, AdSize.SmartBanner, AdPosition.Top);

        // Register for ad events.
        bannerView.OnAdLoaded += HandleAdLoaded;
        bannerView.OnAdFailedToLoad += HandleAdFailedToLoad;
        bannerView.OnAdOpening += HandleAdOpened;
        bannerView.OnAdClosed += HandleAdClosed;
        bannerView.OnAdLeavingApplication += HandleAdLeftApplication;
        // Load a banner ad.
        bannerView.LoadAd(CreateAdRequest());
		bannerView.Hide();
    }

    private void RequestInterstitial()
    {

        // These ad units are configured to always serve test ads.
        #if UNITY_ANDROID
		string adUnitId = NAdManager.Instance.adMobInterAndriodID;
        #elif UNITY_IPHONE
		string adUnitId = NAdManager.Instance.adMobInterIOSID;
		#else
		string adUnitId = "";
		#endif


        // Create an interstitial.
        interstitial = new InterstitialAd(adUnitId);

        // Register for ad events.
        interstitial.OnAdLoaded += HandleInterstitialLoaded;
        interstitial.OnAdFailedToLoad += HandleInterstitialFailedToLoad;
        interstitial.OnAdOpening += HandleInterstitialOpened;
        interstitial.OnAdClosed += HandleInterstitialClosed;
        interstitial.OnAdLeavingApplication += HandleInterstitialLeftApplication;

        // Load an interstitial ad.
        interstitial.LoadAd(CreateAdRequest());
    }

    public void RequestNativeExpressAdView()
    {
        // These ad units are configured to always serve test ads.
        #if UNITY_ANDROID
//        string adUnitId = NAdManager.Instance.adMobNativeID;
        #elif UNITY_IPHONE
//        string adUnitId = NAdManager.Instance.adMobNativeID;
		#else
		string adUnitId = "";
		#endif


        float factorW = (360 / 2.25f);
        float factorH = (140 / 4);
        float factorY = (425 / 4);
        int adsizeW = (int)Mathf.Floor(Screen.width/Screen.dpi * factorW);
        int adsizeH = (int)Mathf.Floor(Screen.height/Screen.dpi * factorH);
        int adsPosY = (int)Mathf.Floor(Screen.height/Screen.dpi * factorY);

        //      NDebug.Instance.text.text = "Aspect : " + Camera.main.aspect +"\n";
        //      NDebug.Instance.text.text += "height : " + Screen.height +"\n";
        //      NDebug.Instance.text.text += "width : " + Screen.width +"\n";
        //      NDebug.Instance.text.text += "dpi : " + Screen.dpi +"\n";
        //      NDebug.Instance.text.text += "Res height : " + Screen.currentResolution.height +"\n";
        //      NDebug.Instance.text.text += "Res width : " + Screen.currentResolution.width +"\n";
        //      NDebug.Instance.text.text += "Screen height : " + Screen.height/Screen.dpi  +"\n";
        //      NDebug.Instance.text.text += "Screen width : " + Screen.width/Screen.dpi  +"\n";
        //      NDebug.Instance.text.text += "adsizeW : " + adsizeW  +"\n";
        //      NDebug.Instance.text.text += "adsizeH : " + adsizeH  +"\n";
        //      NDebug.Instance.text.text += "adsPosY : " + adsPosY  +"\n";

        if (Camera.main.aspect > 1) { 
            return;
        }
        // Create a 320x150 native express ad at the top of the screen.

//        nativeExpressAdView = new NativeExpressAdView(adUnitId, new AdSize(adsizeW, adsizeH), 0, adsPosY);

        // Register for ad events.
//        nativeExpressAdView.OnAdLoaded += HandleNativeExpressAdLoaded;
//        nativeExpressAdView.OnAdFailedToLoad += HandleNativeExpresseAdFailedToLoad;
//        nativeExpressAdView.OnAdOpening += HandleNativeExpressAdOpened;
//        nativeExpressAdView.OnAdClosed += HandleNativeExpressAdClosed;
//        nativeExpressAdView.OnAdLeavingApplication += HandleNativeExpressAdLeftApplication;
//
//        // Load a native express ad.
//        nativeExpressAdView.LoadAd(CreateAdRequest());

    }

    public float DeviceDiagonalSizeInInches ()
    {
        float screenWidth = Screen.width / Screen.dpi;
        float screenHeight = Screen.height / Screen.dpi;
        float diagonalInches = Mathf.Sqrt (Mathf.Pow (screenWidth, 2) + Mathf.Pow (screenHeight, 2));

        Debug.Log ("Getting device inches: " + diagonalInches);

        return diagonalInches;
    }

    public void ShowNative () {
        print("RequestNativeExpressAdView");
        nativeExpressAdView.Show();
    }

    public void HideNative () {
        print("nativeExpressAdView Destroy");
        nativeExpressAdView.Hide();
    }

    private void RequestRewardBasedVideo()
    {
        #if UNITY_ANDROID
//        string adUnitId = NAdManager.Instance.adMobRewardID;
        #elif UNITY_IPHONE
//        string adUnitId = NAdManager.Instance.adMobRewardID;
		#else
		string adUnitId = "";
		#endif

       // rewardBasedVideo.LoadAd(CreateAdRequest(), adUnitId);
    }

    public void ShowInterstitial()
    {
        if (interstitial.IsLoaded())
        {
            interstitial.Show();
        }
        else
        {
            RequestInterstitial();
            MonoBehaviour.print("Interstitial is not ready yet");
        }
    }

    public void ShowRewardBasedVideo()
    {
        if (rewardBasedVideo.IsLoaded())
        {
            rewardBasedVideo.Show();
        }
        else
        {
            MonoBehaviour.print("Reward based video ad is not ready yet");
        }
    }

    #region Banner callback handlers

    public void HandleAdLoaded(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdLoaded event received");
    }

    public void HandleAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        MonoBehaviour.print("HandleFailedToReceiveAd event received with message: " + args.Message);
    }

    public void HandleAdOpened(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdOpened event received");
    }

    public void HandleAdClosed(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdClosed event received");
    }

    public void HandleAdLeftApplication(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdLeftApplication event received");
    }

    #endregion

    #region Interstitial callback handlers

    public void HandleInterstitialLoaded(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleInterstitialLoaded event received");
    }

    public void HandleInterstitialFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        //RequestInterstitial();
        MonoBehaviour.print(
            "HandleInterstitialFailedToLoad event received with message: " + args.Message);
    }

    public void HandleInterstitialOpened(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleInterstitialOpened event received");
    }

    public void HandleInterstitialClosed(object sender, EventArgs args)
    {
        RequestInterstitial();
        MonoBehaviour.print("HandleInterstitialClosed event received");
    }

    public void HandleInterstitialLeftApplication(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleInterstitialLeftApplication event received");
    }

    #endregion

    #region Native express ad callback handlers

    public void HandleNativeExpressAdLoaded(object sender, EventArgs args)
    {
        if (isStartNative) { 
            HideNative();
            isStartNative = false;
        }
        MonoBehaviour.print("HandleNativeExpressAdAdLoaded event received");
    }

    public void HandleNativeExpresseAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        MonoBehaviour.print(
            "HandleNativeExpressAdFailedToReceiveAd event received with message: " + args.Message);
    }

    public void HandleNativeExpressAdOpened(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleNativeExpressAdAdOpened event received");
    }

    public void HandleNativeExpressAdClosed(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleNativeExpressAdAdClosed event received");
    }

    public void HandleNativeExpressAdLeftApplication(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleNativeExpressAdAdLeftApplication event received");
    }

    #endregion

    #region RewardBasedVideo callback handlers

    public void HandleRewardBasedVideoLoaded(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardBasedVideoLoaded event received");
    }

    public void HandleRewardBasedVideoFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        MonoBehaviour.print(
            "HandleRewardBasedVideoFailedToLoad event received with message: " + args.Message);
    }

    public void HandleRewardBasedVideoOpened(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardBasedVideoOpened event received");
    }

    public void HandleRewardBasedVideoStarted(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardBasedVideoStarted event received");
    }

    public void HandleRewardBasedVideoClosed(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardBasedVideoClosed event received");
    }

    public void HandleRewardBasedVideoRewarded(object sender, Reward args)
    {
        string type = args.Type;
        double amount = args.Amount;
        if (NAdManager.Instance.callActionGG != null) NAdManager.Instance.callActionGG();
        MonoBehaviour.print(
            "HandleRewardBasedVideoRewarded event received for " + amount.ToString() + " " + type);
    }

    public void HandleRewardBasedVideoLeftApplication(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardBasedVideoLeftApplication event received");
    }

    #endregion
}
