﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
#if UNITY_IOS
using UnityEngine.iOS;
#endif

public class NTimer : MonoBehaviour
{
	public Button button;
	public string keyTimmer = "";

	private string format = "#00:00:00";
	private DateTime timeNow;
	private DateTime nextStopTime;
	private TimeSpan diffHour;
	private DateTime oldDateNextHour;
	private double f_timeLeft = 0;

	private Action callBack;

	// Use this for initialization
	void Start ()
	{
		CheckTimer();
	}

	public void CheckTimer()
	{
		long temp = Convert.ToInt64(PlayerPrefs.GetString(keyTimmer, "0"));

		oldDateNextHour = DateTime.FromBinary(temp);
		//print(oldDateNextHour);
		nextStopTime = oldDateNextHour;

		if ((oldDateNextHour.Ticks - DateTime.Now.Ticks) > 0)
		{
			StartOldTimer();
		}
		else
		{
			TimeOut();
		}
	}

	public void StartOldTimer()
	{
		if (button != null) button.interactable = false;
		//surprise.GetComponent<Image>().color = new Vector4(1, 1, 1, 0.5f);
		timeNow = DateTime.Now;
		diffHour = oldDateNextHour - timeNow;
		f_timeLeft = diffHour.TotalSeconds;
	}

	// Update is called once per frame
	void Update ()
	{
		TimeLeft();
	}

	void TimeLeft()
	{
		if (isTimeStop) return;  
		//print(f_timeLeft);
		if (f_timeLeft <= 0)
		{
			TimeOut();
		}
		else
		{
			f_timeLeft -= Time.deltaTime;
		}
	}

	void OnApplicationPause(bool isGamePause)
	{
		if (isGamePause)
		{
			timeNow = DateTime.Now;
		}
		else
		{
			f_timeLeft -= ((DateTime.Now - timeNow).TotalSeconds);
		}
	}

	void OnApplicationFocus(bool isGamePause)
	{
		if (!isGamePause)
		{
			timeNow = DateTime.Now;
		}
		else
		{
			f_timeLeft -= ((DateTime.Now - timeNow).TotalSeconds);
		}
	}

	void OnApplicationQuit()
	{
		if (nextStopTime.ToBinary() != 0) {
			//Savee the current system time as a string in the player prefs class
			PlayerPrefs.SetString(keyTimmer, nextStopTime.ToBinary().ToString());
		}
	}

	public void StartTimer(int min, Action callback = null, string format = "")
	{
		if (format != "") this.format = format;
		if (button != null) button.interactable = false;
		callBack = callback;
		//surprise.GetComponent<Image>().color = new Vector4(1, 1, 1, 0.5f);
		timeNow = DateTime.Now;
		nextStopTime = timeNow.AddMinutes(min);
		PlayerPrefs.SetString(keyTimmer, nextStopTime.ToBinary().ToString());
		print("Save time " + nextStopTime);
		diffHour = nextStopTime - timeNow;
		f_timeLeft = diffHour.TotalSeconds;
	}

	public void StartTimerSeconds(int seconds, Action callback = null, string format = "")
	{
		if (format != "") this.format = format;
		if (button != null) button.interactable = false;
		callBack = callback;
		//surprise.GetComponent<Image>().color = new Vector4(1, 1, 1, 0.5f);
		timeNow = DateTime.Now;
		nextStopTime = timeNow.AddSeconds(seconds);
		PlayerPrefs.SetString(keyTimmer, nextStopTime.ToBinary().ToString());
		print("Save time " + nextStopTime);
		diffHour = nextStopTime - timeNow;
		f_timeLeft = diffHour.TotalSeconds;
	}

	public void StartTimer(int min)
	{
		if (format != "") this.format = format;
		if (button != null) button.interactable = false;
		//surprise.GetComponent<Image>().color = new Vector4(1, 1, 1, 0.5f);
		timeNow = DateTime.Now;
		nextStopTime = timeNow.AddMinutes(min);
		PlayerPrefs.SetString(keyTimmer, nextStopTime.ToBinary().ToString());
		print("Save time " + nextStopTime);
		diffHour = nextStopTime - timeNow;
		f_timeLeft = diffHour.TotalSeconds;
	}

	public void TimeOut()
	{
		if (callBack != null) callBack();
		if (button != null) button.interactable = true;
		//surprise.GetComponent<Image>().color = new Vector4(1, 1, 1, 1f);
	}
	bool isTimeStop = false;
	public void StopTime () {
		isTimeStop = true;
	}

	public void ResumeTime () {
		isTimeStop = false;
	}

	void ScheduleNotificationForiOSWithMessage(string text, System.DateTime fireDate)
	{
		#if UNITY_IOS
		if (Application.platform == RuntimePlatform.IPhonePlayer)
		{
			UnityEngine.iOS.LocalNotification notification = new UnityEngine.iOS.LocalNotification();
			notification.fireDate = fireDate;
			notification.alertAction = "Alert";
			notification.alertBody = text;
			notification.hasAction = false;
			UnityEngine.iOS.NotificationServices.ScheduleLocalNotification(notification);


			UnityEngine.iOS.NotificationServices.RegisterForNotifications ( NotificationType.Alert | 
				NotificationType.Badge | 
				NotificationType.Sound);

		}
		#endif
	}
}
