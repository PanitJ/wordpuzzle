﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using K2.Popup;

public class SplashSceneController : MonoBehaviour {

    [SerializeField] private Sprite _languageEN;
    [SerializeField] private Sprite _languageTH;
    [SerializeField] private Sprite _titleTH;
    [SerializeField] private Sprite _titleEN;

    [SerializeField] private Image _soundImage;
    [SerializeField] private Image _musicImage;
    [SerializeField] private Image _languageImage;
    [SerializeField] private Image _titleImage;
    [SerializeField] private Text _playText;
    [SerializeField] private Text _titleText;

    [SerializeField] private SettingsPopup _settingsPopup;
    [SerializeField] private HelpPopup helpPopup;

    private string _nextScene = "Mode";

    private void Start()
    {
        _nextScene = Profile.Instance.hasLoadData ? "Game" : "Mode";
        SoundManager.instance.PlayMenuMusic();
    }

    public void Play()
    {
        SceneManager.LoadScene(_nextScene);
        SoundManager.instance.PlayButtonSound();
        FireBaseManager.instance.LogPlay();
    }

    public void Settings()
    {
        SoundManager.instance.PlayButtonSound();
        var popup = _settingsPopup.CloneAndDeploy();
        PopupSystem.Instance.Show();
        PopupSystem.Instance.Add(popup);
    }
}
