﻿using K2.Popup;
using UnityEngine;
using System;

public class IAPPopup : PopupContent
{
    [SerializeField] private IAPManager _iAPManager;
    [SerializeField] private RectTransform _rect;
    [SerializeField] private RectTransform _header;

    [SerializeField] private GameObject _promotionHolder;
    [SerializeField] private GameObject _commonShopHolder;
    [SerializeField] private GameObject _specialShop_1_Holder;
    [SerializeField] private GameObject _specialShop_2_Holder;
    [SerializeField] private GameObject _specialShop_3_Holder;
    [SerializeField] private GameObject _specialShop_4_Holder;
    [SerializeField] private GameObject _specialShop_5_Holder;
    [SerializeField] private GameObject _specialShop_6_Holder;


    private const float commonHeight = 1000f;
    private const float specialShop_1_Height = 1000f;
    private const float specialShop_2_Height = 1000f;
    private const float specialShop_3_Height = 1000f; 
    private const float specialShop_4_Height = 1000f;
    private const float specialShop_5_Height = 1000f;
    private const float specialShop_6_Height = 1000f;

    private const float headerOffset = 200f;
    private const float width = 540f;

    public HeaderBarController HeaderBarController;
    public enum IAPType {Common,Special1 = 1,Special2 = 2,Special3 = 3, Special4 = 4, Special5 = 5, Special6 = 6}

    public void Deploy(IAPType type)
    {
        _commonShopHolder.gameObject.SetActive(false);
        _specialShop_1_Holder.gameObject.SetActive(false);
        _specialShop_2_Holder.gameObject.SetActive(false);
        _specialShop_3_Holder.gameObject.SetActive(false);
        _specialShop_4_Holder.gameObject.SetActive(false);
        _specialShop_5_Holder.gameObject.SetActive(false);
        _specialShop_6_Holder.gameObject.SetActive(false);

        switch (type)
        {
            case IAPType.Common:
                DeployCommonShop();
                AdjustHeight(commonHeight);
                break;
            case IAPType.Special1:
                _specialShop_1_Holder.gameObject.SetActive(true);
                AdjustHeight(specialShop_1_Height);
                break;
            case IAPType.Special2:
                _specialShop_2_Holder.gameObject.SetActive(true);
                AdjustHeight(specialShop_2_Height);
                break;
            case IAPType.Special3:
                _specialShop_3_Holder.gameObject.SetActive(true);
                AdjustHeight(specialShop_3_Height);
                break;
            case IAPType.Special4:
                _specialShop_4_Holder.gameObject.SetActive(true);
                AdjustHeight(specialShop_4_Height);
                break;
            case IAPType.Special5:
                _specialShop_5_Holder.gameObject.SetActive(true);
                AdjustHeight(specialShop_5_Height);
                break;
            case IAPType.Special6:
                _specialShop_6_Holder.gameObject.SetActive(true);
                AdjustHeight(specialShop_6_Height);
                break;
        }
    }

    private void DeployCommonShop ()
    {
        _commonShopHolder.gameObject.SetActive(true);
        _promotionHolder.gameObject.SetActive(Profile.Instance.OfferNumber > 0);
    }

    public void OnTapPromotion ()
    {
        IAPType type = (IAPType) Profile.Instance.OfferNumber;
        Deploy(type);
       // PopupSystem.Instance.Show();
       // var content = Instantiate(this);
      //  PopupSystem.Instance.Add(this);
    }

    private void AdjustHeight(float height)
    {
        _rect.sizeDelta = new Vector2(width,height);
        _header.localPosition = new Vector2(0, headerOffset - ((commonHeight - height) / 2f));
    }

    public void Close()
    {
        SoundManager.instance.PlayButtonSound();
        PopupSystem.Instance.CloseLayer();
    }

    public void CloseOffer ()
    {
        Close();
    }

    public void BuyPK1()
    {
        BuyCoinSetUp();
        _iAPManager.BuyCoinPK1();
    }

    public void BuyPK2()
    {
        BuyCoinSetUp();
        _iAPManager.BuyCoinPK2();
    }


    public void BuyPK3()
    {
        BuyCoinSetUp();
        _iAPManager.BuyCoinPK3();
    }


    public void BuyPK4()
    {
        BuyCoinSetUp();
        _iAPManager.BuyCoinPK4();
    }


    public void BuyPK5()
    {
        BuyCoinSetUp();
        _iAPManager.BuyCoinPK5();
    }

    public void BuyPK6()
    {
        BuyCoinSetUp();
        _iAPManager.BuyCoinPK6();
    }

    public void BuySpecialPack1()
    {
        BuyOfferSetUp();
        _iAPManager.BuySpecial1();
    }

    public void BuySpecialPack2()
    {
        BuyOfferSetUp();
        _iAPManager.BuySpecial2();
    }

    public void BuySpecialPack3()
    {
        BuyOfferSetUp();
        _iAPManager.BuySpecial3();
    }

    public void BuySpecialPack4()
    {
        BuyOfferSetUp();
        _iAPManager.BuySpecial4();
    }

    public void BuySpecialPack5()
    {
        BuyOfferSetUp();
        _iAPManager.BuySpecial5();
    }

    public void BuySpecialPack6()
    {
        BuyOfferSetUp();
        _iAPManager.BuySpecial6();
    }

    public void WatchRewardVideo () {
		IAPManager.Instance.ShowWait();
		NAdManager.Instance.ShowAdRewardUnity( ()=> {
			IAPManager.Instance.HideWait();
            Profile.Instance.Gold += 25;
            if (HeaderBarController != null)
                HeaderBarController.UpdateGold();
        });
	}

    void BuyCoinSetUp ()
    {
        SoundManager.instance.PlayButtonSound();
        _iAPManager.OnSuccess = null;
        _iAPManager.OnFail = null;
        _iAPManager.OnSuccess += OnBuySuccess;
        _iAPManager.OnFail += OnBuyFail;
    }


    void BuyOfferSetUp()
    {
        SoundManager.instance.PlayButtonSound();
        _iAPManager.OnSuccess = null;
        _iAPManager.OnFail = null;
        _iAPManager.OnSuccess += OnOfferSuccess;
        _iAPManager.OnFail += OnOfferFail;
    }

    void OnOfferSuccess ()
    {
        if (HeaderBarController != null)
            HeaderBarController.UpdateGold();
        Profile.Instance.OfferNumber = 0;
        CloseOffer();
    }


    void OnOfferFail()
    {
        OnBuyFail();
    }
    void OnBuySuccess ()
    {
        if (HeaderBarController != null)
            HeaderBarController.UpdateGold();
        Close();
    }

    void OnBuyFail ()
    {
        HeaderBarController.DeployError("ERROR Unable to connect IAP.");
    }
}
 