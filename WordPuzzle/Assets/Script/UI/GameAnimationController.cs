﻿using  UnityEngine;
using UnityEngine.UI;

public class GameAnimationController : MonoBehaviour
{

    [SerializeField] private GameObject textPopupsHolder;
    [SerializeField] private Text textPopups;

    private const float EXECUTE_TIME = 0.75f;
    private const float BACKWARD_TIME = 0.5f;
    private const float MOVE_UP_POSITION = 100f;

    public void SetVisible (bool visible)
    {
        textPopupsHolder.SetActive(visible);
    }

    public void DeployTextPopup(string message)
    {
        if (!textPopupsHolder.activeInHierarchy)
            SetVisible(true);
        textPopups.text = message;
        LeanTween.scale(textPopupsHolder, Vector2.one, EXECUTE_TIME);
        LeanTween.moveLocalY(textPopupsHolder, MOVE_UP_POSITION, EXECUTE_TIME/2f).setDelay(EXECUTE_TIME/2f);
        LeanTween.scale(textPopupsHolder, Vector2.zero, BACKWARD_TIME).setDelay(EXECUTE_TIME);
        LeanTween.moveLocalY(textPopupsHolder, 0f, BACKWARD_TIME).setDelay(EXECUTE_TIME * 2);
    }
}
