﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
public class NToolMenu : MonoBehaviour {

#if UNITY_EDITOR

    [MenuItem("NTool/Add Ads")]
    static void DoSomething()
    {
        GameObject go = new GameObject("NAdManager");
        go.AddComponent<NAdManager>();
    }

	[MenuItem("NTool/Open Finder SS")]
	static void OpenFinderSS()
	{
		System.Diagnostics.Process.Start(Application.persistentDataPath);
	}

#endif


}
