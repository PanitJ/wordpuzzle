﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Level : MonoBehaviour {
    [SerializeField] private Text _level;
    [SerializeField] private Text _status;
    [SerializeField] private Image _badge;

    [SerializeField] private Text _levelPreview;

    [SerializeField] private Button _button;
    private int _index;
    private int _mode;
    [SerializeField] Sprite _solve;
    [SerializeField] Sprite _unSolve;
    [SerializeField] Sprite _lock;
    [SerializeField] Sprite _unlock;
    [SerializeField] LetterPreview _letterPreview;
    [SerializeField] GameObject _cirecle;
    [SerializeField] Image plate;


    public void Calibrate(int index, bool interactable,float beginY,float gapY,int mode)
    {
        //if (index >= _levelActive.Length)
        //    return;
        _mode = mode;
        _index = index;
        _button.interactable = interactable;
        _level.text = "Level " + (index+1);
        if (interactable)
        {
           // _level.sprite = _levelActive[index];
            // _status.sprite = _solve;
            _status.text = "Solved";
           
            if(_mode == LevelManager.instance.MaxProgress && index == LevelManager.instance.MaxSubProgress)
                     _status.text = "Current";

            char[] letters =  LevelManager.instance.GetSubLevel(index);
            //string preview = "";
            //for (int i = 0; i < letters.Length; i++)
            //{
            //    preview = string.Concat(preview, char.ToUpper(letters[i]));
            //    if (i < letters.Length - 1)
            //        preview = string.Concat(preview, " ");
            //}
            //_levelPreview.text = preview;
            CalibrateCircle(letters);
            //  _badge.sprite = _unlock;
            if (_badge.gameObject.activeInHierarchy)
            _badge.gameObject.SetActive(false);
            plate.color = Color.white;
        }
        else
        {
            _levelPreview.text = "";
            _status.text = "Unsolved";
            _badge.gameObject.SetActive(true);
            _badge.sprite = _lock;
            plate.color = new Color(0.8f, 0.8f, 0.8f);
            _level.color = Color.black;

        }
        transform.localPosition = new Vector3(0,beginY + (_index * -gapY),0);

    }

    float circleRadius = 40f;
    float letterRadius = 17f;
    int fontSize = 25;
    public void CalibrateCircle (char[] letters)
    {
        for (int i = 0; i < letters.Length; i++)
        {
            var angle = i * Mathf.PI * 2 / letters.Length;
            var pos = new Vector3(Mathf.Sin(angle), Mathf.Cos(angle), 0) * (circleRadius - letterRadius);
            var newNode = Instantiate<LetterPreview>(_letterPreview, _cirecle.transform);
            newNode.Calibrate(letters[i],fontSize);
            newNode.transform.localPosition = pos;
        }
    }

    public void GotoLevel()
    {
        Profile.Instance.CurrentLevelData.currentSubLevel = _index;
        SceneManager.LoadScene("Game");
        FireBaseManager.instance.LogModeSelect(_index);
    }
}
