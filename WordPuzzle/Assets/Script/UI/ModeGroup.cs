﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ModeGroup : MonoBehaviour {

    [SerializeField] private Text text;
    [SerializeField] private Image icon;
    [SerializeField] private RectTransform rect;
    [SerializeField] private GameObject holder;
    [SerializeField] private Mode _modePrefab;
    [SerializeField] private RectTransform rectBG;
    [SerializeField] private GameObject plate;

    const float Half_icon_height = 160f;
    const float Width = 500f;
    public const float Base_Height = 100f;
    public const float Content_Height = 120f;
    const float Base_Holder_Position = -5f;
    const float Holder_Gap_Factor = 60f;

    int beginIndex = 0;

    public void Calibrate(LevelData[] levelDatas, int beginIndex)
    {
        this.beginIndex = beginIndex;
        Debug.Log("lenght " + levelDatas.Length + " varaint " + Profile.Instance.Variant);
        name = levelDatas[0].ThemeName;
        text.text = name;
        icon.sprite = levelDatas[0].iconSprite;
        SetSize(levelDatas.Length);
        SetIconPosition();
        SetHolderPosition(levelDatas.Length);
        for (int i = 0; i < levelDatas.Length; i++)
        {
            CreateChild(levelDatas[i],i);
        }
    }

    void SetSize(int count)
    {
        rect.sizeDelta = new Vector3(Width, Base_Height + (count * Content_Height), 0);
        rectBG.sizeDelta = rect.sizeDelta;
    }

    void SetIconPosition()
    {
        plate.transform.localPosition = new Vector3(0, Height / 2f + 80f, 0);
        //icon.transform.localPosition = new Vector3(0, rect.sizeDelta.y / 2f - Half_icon_height, 0);
    }

    void SetHolderPosition(int count)
    {
        holder.transform.localPosition = new Vector3(0, Base_Holder_Position + (Holder_Gap_Factor * count), 0);
    }

    void CreateChild(LevelData levelData, int index)
    {
        var newMode = Instantiate<Mode>(_modePrefab, holder.transform);
        newMode.transform.localPosition = new Vector3(0, index * -Content_Height, 0);
        newMode.Calibrate(levelData, beginIndex + index);
        Button button = newMode.GetComponent<Button>();
        button.interactable = (newMode.levelData.mainLevel <= LevelManager.instance.MaxProgress);
    }

    public float Height {
        get
        {
            return  rect.sizeDelta.y;
        }
    }

}
