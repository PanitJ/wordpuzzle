﻿using UnityEngine;

public class SoundManager : MonoBehaviour {

    public AudioSource efxSource;                   //Drag a reference to the audio source which will play the sound effects.
    public AudioSource musicSource;                 //Drag a reference to the audio source which will play the music.
    public static SoundManager instance = null;
    [SerializeField] private AudioClip _buttonSound;
    [SerializeField] private AudioClip _winSound;
    [SerializeField] private AudioClip _matchSound;

    [SerializeField] private AudioClip _menuMusic;
    [SerializeField] private AudioClip _gameMusic;

    private const string SOUND_KEY = "SOUND_KEY";
    private const string MUSIC_KEY = "MUSIC_KEY";

    void Awake()
    {
        //Check if there is already an instance of SoundManager
        if (instance == null)
            //if not, set it to this.
            instance = this;
        //If instance already exists:
        else if (instance != this)
            //Destroy this, this enforces our singleton pattern so there can only be one instance of SoundManager.
            Destroy(gameObject);

        //Set SoundManager to DontDestroyOnLoad so that it won't be destroyed when reloading our scene.
        DontDestroyOnLoad(gameObject);
    }


    public void PlayMenuMusic ()
    {
        if (musicSource.clip == _menuMusic) return;
        musicSource.clip = _menuMusic;
        musicSource.Play();
    }

    public void PlayGameMusic ()
    {
        if (musicSource.clip == _gameMusic) return;
        musicSource.clip = _gameMusic;
        musicSource.Play();
    }

    private void Start()
    {
        UpdateSetting();
    }

    public bool IsSoundMuted()
    {
        return !IsSoundEnabled();
    }

    public bool IsMusicMuted()
    {
        return !IsMusicEnabled();
    }


    public bool IsSoundEnabled()
    {
        return bool.Parse(PlayerPrefs.GetString(SOUND_KEY, "true"));
    }

    public void SetSoundEnabled(bool enabled)
    {
        PlayerPrefs.SetString(SOUND_KEY, enabled.ToString());
        UpdateSetting();
    }

    public bool IsMusicEnabled()
    {
        return bool.Parse(PlayerPrefs.GetString(MUSIC_KEY, "true"));
    }

    public void SetMusicEnabled(bool enabled)
    {
        PlayerPrefs.SetString(MUSIC_KEY, enabled.ToString());
        UpdateSetting();
    }

    public void PlayButtonSound()
    {
        efxSource.PlayOneShot(_buttonSound);
    }

    public void PlayWinSound()
    {
        efxSource.PlayOneShot(_winSound);
    }

    public void PlayMatchSound()
    {
        efxSource.PlayOneShot(_matchSound);
    }

    public void UpdateSetting()
    {
        efxSource.mute = IsSoundMuted();
        musicSource.mute = IsMusicMuted();
    }
}
