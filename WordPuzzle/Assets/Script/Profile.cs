﻿using System.Collections.Generic;
using UnityEngine;
using K2.Popup;
using System;

public class Profile : MonoBehaviour
{
    public const int HINT_COST = 25;
    public const int GOLD_PER_LEVEL = 50;
    public const int EXTRA_WORD_BONUS = 100;

    public static Profile _instance;
    private TextAsset _asset;
    private int _gold = 500;
    private IList<string[]> _subLevelList;
    private DataController _dataController;
    public bool hasLoadData = false;
    public int Variant = 0;
    private int _offerNumber = 0;
    private bool _canOffer = false;

    const int version = 6;

    public Profile()
    {

    }

    private void RandomVariant ()
    {
        Variant = K2.Dice.Range(1, 4);
        CanOffer = true;
        PlayerPrefs.SetInt("VARIANT", Variant);
    }

    public int OfferNumber
    {
        get {
            return _offerNumber;
        }
        set{
            _offerNumber = value;
            PlayerPrefs.SetInt("OFFER", _offerNumber);
        }
    }

    public bool CanOffer
    {
        get
        {
            return _canOffer;
        }
        set
        {
            _canOffer = value;
            PlayerPrefs.SetString("CANOFFER", _canOffer.ToString());
        }
    }

    public static Profile Instance
    {
        get
        {
            if (_instance == null)
            {
                var container = new GameObject("ProfileController");
                _instance = container.AddComponent<Profile>();
                _instance.Init();
                DontDestroyOnLoad(container);
            }
            return _instance;
        }
    }

    public LevelData CurrentLevelData { get; set; }

    public int CurrentSubLevel { get; set; }
    public int CurrentMainLevel { get; set; }

    public int MaxSubLevel { get; set; }
    public int MaxMainLevel { get; set; }

    public List<string> CurrentPlayedWords { get; set; }
    public List<string> HavePlayedExtrawords { get; set; }
    public string CurrentTextAssetName { get; set; }

    public int Gold
    {
        get { return _gold; }
        set { _gold = value; }
    }

    private void Init()
    {
        CurrentPlayedWords = new List<string>();
        HavePlayedExtrawords = new List<string>();
        _dataController = new DataController();
        LoadStateVariable();
        LevelManager.instance.Load();
        if (PlayerPrefs.GetInt("VERSION", 1) != version)
        {
            hasLoadData = false;
            RandomVariant();
        }
        if (hasLoadData)
        {
            LevelManager.instance.SetMaxProgress(MaxMainLevel, MaxSubLevel);
            CurrentLevelData = LevelManager.instance.GetMainLevel(CurrentMainLevel);
            CurrentLevelData.currentSubLevel = CurrentSubLevel;
           // LevelManager.instance.ShowOfferFirstTime();
        }
        else
            LevelManager.instance.IsStarReady = true;
    }
            
    private void LoadStateVariable ()
    {
        bool canOffer = false;
        Variant = PlayerPrefs.GetInt("VARIANT", 0);
        _offerNumber = PlayerPrefs.GetInt("OFFER", 0);
        if (bool.TryParse(PlayerPrefs.GetString("CANOFFER"), out canOffer))
            _canOffer = canOffer;
        if (Variant == 0)
            RandomVariant();
    }

    public static void Save()
    {
        Debug.Log("");
        PlayerPrefs.SetInt("VERSION", version);
        _instance._dataController.Save();   
    }


}