﻿using System.Collections.Generic;
using UnityEngine;
using System;

public class LevelData {

    public Sprite iconSprite;
    public Sprite iconSpriteActive;

    public Sprite nameSprite;
    private IList<string[]> _subLevelList;
    private TextAsset _asset;
    private int _maxLevel = 0;
    public int currentSubLevel; //SubLevel
    public int mainLevel; //MainLevel
    public string name;


    public void Init(TextAsset asset)
    {
        Asset = asset;
    }

    public void Calibrate(Sprite iconSprite, Sprite iconSpriteActive, Sprite nameSprite, int index, bool hasClear, int currentLevel = 0)
    {
        this.mainLevel = index;
        this.iconSprite = iconSprite;
        this.iconSpriteActive = iconSpriteActive;
        this.nameSprite = nameSprite;
        if (hasClear)
            Complete();
        else
            this.currentSubLevel = currentLevel;
    }

    public void Complete()
    {
        _maxLevel = SubLevelList.Count;
    }

    public void Progress()
    {
        currentSubLevel++;
        if (currentSubLevel > _maxLevel)
            _maxLevel++;
    }

    public int MaxLevel {
        get
        {
            if (_maxLevel > SubLevelList.Count - 1)
                return SubLevelList.Count - 1;
            return _maxLevel;
        }
        set
        {
            _maxLevel = value;
        }
    }

    //public float PercentProgress {
    //    get
    //    {
    //        return maxLevel / (SubLevelList.Count - 1);
    //    }
    //}

    public bool HasClear {
        get
        {
            Debug.Log(_maxLevel + "   " + SubLevelList.Count);
            return _maxLevel >= SubLevelList.Count;
        }
    }

    public IList<string[]> SubLevelList
    {
        get { return _subLevelList ?? (_subLevelList = new List<string[]>()); }
        set { _subLevelList = value; }
    }

    public char[] LetterListOfIndex(int index) {

        string[] letters = SubLevelList[index];
        return letters[0].ToCharArray();
    }


    public TextAsset Asset
    {
        get { return _asset; }
        set
        {
            _asset = value;
            this.name = _asset.name;
            string rawText = _asset.text.Replace(' ', ',');
            SubLevelList = CSVParser.ReadAsList(rawText);
            //themeNumber = int.Parse(SubLevelList[0][0]);
            if (Enum.IsDefined(typeof(LevelManager.Theme), SubLevelList[0][0]))
               theme = (LevelManager.Theme)Enum.Parse(typeof(LevelManager.Theme), SubLevelList[0][0]);
            this.name = SubLevelList[1][0];

            SubLevelList.RemoveAt(0);
            SubLevelList.RemoveAt(0);
        }
    }

    LevelManager.Theme theme = LevelManager.Theme.Aquatic;

    public string ThemeName {
        get
        {
            return theme.ToString();
        }
    }

    public int ThemeNumber {
        get
        {
            return (int)theme;
        }
    }
}
