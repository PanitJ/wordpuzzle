﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.Advertisements;

public class AdManagerUnity : MonoBehaviour
{

    string idAds = "";
    bool isRemoveAds = false;
    public bool isTest = false;

    private Action callBack = null;

    private static GameObject parentObject;

    private static AdManagerUnity _Singleton;

    public static AdManagerUnity Instance
    {
        get
        {
            if (!_Singleton)
            {
                parentObject = new GameObject("AdManagerUnity");
                DontDestroyOnLoad(parentObject);
                _Singleton = parentObject.AddComponent<AdManagerUnity>();
                //_Singleton.Initialize();
            }
            return _Singleton;
        }
    }

    void Start ()
    {
        //Instance.Initialize();
    }

    public void Initialize()
    {
		
		#if UNITY_ANDROID
		idAds = NAdManager.Instance.unityAndriodID;
		#elif UNITY_IPHONE
		idAds = NAdManager.Instance.unityIOSID;
		#endif
//		print(idAds);
        if (!Advertisement.isInitialized)
            Advertisement.Initialize(idAds, false);
    }

    public void ShowAdReward(string zone = "", Action callBackAction = null)
    {
        Debug.Log("ShowAd HC");
        StartCoroutine(WaitForAd());

        if (string.Equals(zone, ""))
            zone = null;

        ShowOptions options = new ShowOptions();
        options.resultCallback = AdCallbackReward;

        if (Advertisement.IsReady())
        {
            callBack = callBackAction;
            Advertisement.Show("rewardedVideo", options);
            Advertisement.Initialize(idAds, false);
        }
    }

    public void ShowAdNormal(string zone = "")
    {
        Debug.Log("ShowAd Normal");
        StartCoroutine(WaitForAd());

        if (string.Equals(zone, ""))
            zone = null;

        ShowOptions options = new ShowOptions();
        options.resultCallback = AdCallbackhandler;

        if (Advertisement.IsReady() && !Instance.isRemoveAds)
        {
            Advertisement.Show("", options);
            Advertisement.Initialize(idAds, false);
        }
    }

    void AdCallbackhandler(ShowResult result)
    {
        Debug.Log("Call Back");
        switch (result)
        {
            case ShowResult.Finished:
                Debug.Log("Ad Finished. Rewarding player...");
                break;
            case ShowResult.Skipped:
                Debug.Log("Ad skipped. Son, I am dissapointed in you");
                break;
            case ShowResult.Failed:
                Debug.Log("I swear this has never happened to me before");
                break;
        }
        NAdManager.Instance.ResetCountAds();
    }

    void AdCallbackReward(ShowResult result)
    {
        Debug.Log("Call Back");
        switch (result)
        {
            case ShowResult.Finished:
//				NWaiting.Instance.HideWaiting();
                if (callBack != null) callBack();
                Debug.Log("Ad Finished. Rewarding player...");
                break;
            case ShowResult.Skipped:
//				NWaiting.Instance.HideWaiting();
                Debug.Log("Ad skipped. Son, I am dissapointed in you");
                break;
            case ShowResult.Failed:
//				NWaiting.Instance.HideWaiting();
                //Advertisement.Initialize (gameID, isTest);
                Debug.Log("I swear this has never happened to me before");
                break;
        }

    }

    IEnumerator WaitForAd()
    {

        float currentTimeScale = Time.timeScale;
        Time.timeScale = 0f;
        yield return null;

        while (Advertisement.isShowing)
            yield return null;

        Time.timeScale = currentTimeScale;
    }
		
}