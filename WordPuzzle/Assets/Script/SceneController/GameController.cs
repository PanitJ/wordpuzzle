﻿using System;
using UnityEngine;
using K2.Popup;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    [SerializeField] private WordCircleView _wordCircleView;
    [SerializeField] private AnswerController answerController;
    [SerializeField] char[] alphabets;
    [SerializeField] private HeaderBarController _headerBarController;
    [SerializeField] private GameAnimationController _gameAnimationController;
    [SerializeField] private Compliment _compliment;
    [SerializeField] private ParticleSystem _particleSystem;


    private const float CHECK_ANSWER_TIME = 0.25f;
    private const float END_GAME_DELAY = 0.5f;
    private string[] thisLevelStrings;

    const int MAX_ANSWERS = 16;

    private void Start ()
	{
        _wordCircleView.gameController = this;
        Calibrate();
        SoundManager.instance.PlayGameMusic();
    }

    void OnApplicationQuit()
    {
        FireBaseManager.instance.LogExit(Profile.Instance.Variant, LevelManager.instance.MaxProgress);
        Profile.Save();
    }

    private void Calibrate()
    {
        _gameAnimationController.SetVisible(false);
        int level = Profile.Instance.CurrentLevelData.currentSubLevel;
        //    thisLevelStrings = Profile.Instance.SubLevelList[level];
        thisLevelStrings = Profile.Instance.CurrentLevelData.SubLevelList[level];

        Debug.Log("this level string = " + thisLevelStrings.Length);
        //alphabets = thisLevelStrings[1].ToCharArray();
        alphabets = thisLevelStrings[0].ToCharArray();

        _wordCircleView.Calibrate(alphabets);
        _headerBarController.Calibrate(BackToMenu, true);
        GenerateAnswer(thisLevelStrings);
        PutCurrentPlayWords();
    }

    void PutCurrentPlayWords()
    {
        if (global::Profile.Instance.CurrentPlayedWords == null || global::Profile.Instance.CurrentPlayedWords.Count == 0) return;
        for (int i = 0; i < global::Profile.Instance.CurrentPlayedWords.Count; i++)
            answerController.CheckAnswer(global::Profile.Instance.CurrentPlayedWords[i]);
    }

    void BackToMenu()
    {
        Profile.Save();
        SceneManager.LoadScene("Mode");
    }

    private void GenerateAnswer(string[] alphabetStrings)
    {
        int extraWordLength = alphabetStrings.Length - 1 - MAX_ANSWERS;
        if (extraWordLength < 0) extraWordLength = 0;
        string[] extraWordString = new string[extraWordLength];
        string[] answersString = new string[alphabetStrings.Length  -  extraWordLength -1];

        //  string[] extraWordString = new string[extraWordLength];
        Debug.Log("answer count = " + answersString.Length);
        int j = 0;
        for (int i = extraWordLength; i < answersString.Length + extraWordLength; i++)
        {
            //   answersString[i] = alphabetStrings[i + 2];
            answersString[j] = alphabetStrings[i + 1];
            j++;
        }

        for (int i = 0; i < extraWordLength; i++)
        {
            extraWordString[i] = alphabetStrings[i + 1];
            Debug.Log(extraWordString[i]);
        }


        int starWordIndex = -1;
        if(LevelManager.instance.IsStarReady)
            starWordIndex = UnityEngine.Random.Range(0, answersString.Length);
       // answerController.Calibrate(answersString, alphabets.Length,extraWordString, starWordIndex);
        answerController.Calibrate(answersString, alphabets.Length, null, starWordIndex);

    }


    void OnStarComplete ()
    {
        answerController.hasBonus = false;
        _gameAnimationController.DeployTextPopup("BONUS + 5 GOLD !!!");
        Profile.Instance.Gold += 5;
        _headerBarController.UpdateGold();
        LevelManager.instance.StartTimerForStar();
    }

    void OnExtrawordComplete()
    {
        _gameAnimationController.DeployTextPopup("EXTRAWORD BONUS + 3 GOLD !!!");
        Profile.Instance.Gold += 3;
        _headerBarController.UpdateGold();
    }

    public Color CheckAnswer(string selectedString)
    {
        K2.Time.Add(Reset, delayInSeconds: CHECK_ANSWER_TIME, repeatCount: 1);
        var isWordValid = answerController.CheckAnswer(selectedString.ToLower());
        var isExtraWordValid = answerController.CheckExtraAnswer(selectedString.ToLower());

        if (isExtraWordValid)
        {
            SoundManager.instance.PlayMatchSound();
            OnExtrawordComplete();
       //     _headerBarController.DeployExtraWordBonus(selectedString);
            return Color.blue;
        }

        if (!isWordValid)
            return Color.red;

        if (!answerController.IsLevelComplete())
        {
            ///Answer that already done
            if (Profile.Instance.CurrentPlayedWords.Contains(selectedString))
                return Color.yellow;

            ///Correct answer
            PlayMatchSoundWithParticle();
            Profile.Instance.CurrentPlayedWords.Add(selectedString);
            _compliment.Show(Compliment.Type.GoodJob);
            if (answerController.hasBonus) K2.Time.Add(OnStarComplete, 0.5f, 1) ;

            return Color.green;
        }

        ///Complete Level
        PlayMatchSoundWithParticle();
        EndLevel();
        return Color.magenta;
    }

    void PlayMatchSoundWithParticle ()
    {
        SoundManager.instance.PlayMatchSound();
        _particleSystem.Play();
    }


    private void EndLevel()
    {
        InputController.Lock();
        _compliment.Show(Compliment.Type.WellDone);
        K2.Time.Add(End, END_GAME_DELAY, 1);   
    }

    void End()
    {
        InputController.Unlock();
        SoundManager.instance.PlayWinSound();
       // _wordCircleView._label.text = "Completed";
        DeployPopUpEnd();
        LevelManager.instance.Progress();
    }

    private void DeployPopUpEnd()
    {
        _headerBarController.DeployPopUpEnd(Profile.Instance.CurrentLevelData.SubLevelList.Count, Profile.Instance.CurrentLevelData.mainLevel, Profile.Instance.CurrentLevelData.currentSubLevel);
    }

    private void Reset()
    {
        _wordCircleView.Reset();
    }

    public void HintOption ()
    {
        LevelManager.instance.ShowOffer();
    }

    public void Hint()
    {
        if (InputController.IsPause) return;
        InputController.Lock();

        SoundManager.instance.PlayButtonSound();
        if (Profile.Instance.Gold < Profile.HINT_COST)
        {
            InputController.Unlock();
            if(Profile.Instance.CanOffer)
                LevelManager.instance.ShowOfferFirstTime();
            else
                _headerBarController.DeployErrorWithOption("Not enought gold to hint.", "Get promotion", HintOption);
        }
        else
        {
            answerController.Hint();
            _headerBarController.UpdateGold();
            if (answerController.IsLevelComplete())
            {
                EndLevel();
            }
            else
            {
                K2.Time.Add(UnlockShuffle, LetterView.AnimationTime, 1);
            }
        }
        FireBaseManager.instance.LogHint(Profile.Instance.Variant,Profile.Instance.CurrentLevelData.mainLevel, Profile.Instance.CurrentLevelData.currentSubLevel);
    }

    public const float SHUFFLE_TIME = 0.15F;
    bool isShufflering = false;
    public void Shuffle ()
    {
        if (InputController.IsPause) return;
        InputController.Lock();

        if (isShufflering) return;
        isShufflering = true;
        SoundManager.instance.PlayButtonSound();
        Debug.Log("Shuffle");
        _wordCircleView.Shuffle();
        K2.Time.Add(UnlockShuffle, SHUFFLE_TIME, 1);
    }

    void UnlockShuffle ()
    {
        isShufflering = false;
        InputController.Unlock();
    }


}
