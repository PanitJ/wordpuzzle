﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;

public class IAPManager : MonoBehaviour, IStoreListener
{
    private static IStoreController m_StoreController;          // The Unity Purchasing system.
    private static IExtensionProvider m_StoreExtensionProvider; // The store-specific Purchasing subsystems.
    public Action OnSuccess;
    public Action OnFail;
    [SerializeField] GameObject _pleaseWait;
    // Product identifiers for all products capable of being purchased: 
    // "convenience" general identifiers for use with Purchasing, and their store-specific identifier 
    // counterparts for use with and outside of Unity Purchasing. Define store-specific identifiers 
    // also on each platform's publisher dashboard (iTunes Connect, Google Play Developer Console, etc.)

    // General product identifiers for the consumable, non-consumable, and subscription products.
    // Use these handles in the code to reference which product to purchase. Also use these values 
    // when defining the Product Identifiers on the store. Except, for illustration purposes, the 
    // kProductIDSubscription - it has custom Apple and Google identifiers. We declare their store-
    // specific mapping to Unity Purchasing's AddProduct, below.
    //public static string COINS_50 = "coins50"; 
    public static string COINS_125 = "coinpackage1";
    public static string COINS_250 = "coinpackage2";
    public static string COINS_690 = "coinpackage3";
    public static string COINS_1500 = "coinpackage4";
    public static string COINS_3250 = "coinpackage5";
    public static string COINS_8750 = "coinpackage6";

    public static string OFFER_1_1 = "com.manawings.wordsafari.promotion1_1";
    public static string OFFER_1_2 = "com.manawings.wordsafari.promotion1_2";
    public static string OFFER_2_1 = "com.manawings.wordsafari.promotion2_1";
    public static string OFFER_2_2 = "com.manawings.wordsafari.promotion2_2";
    public static string OFFER_3_1 = "com.manawings.wordsafari.promotion1_1_time";
    public static string OFFER_4_1 = "com.manawings.wordsafari.promotion2_1_time";

    public static IAPManager Instance;

    void Start()
    {
		Instance = this;
        // If we haven't set up the Unity Purchasing reference
		HideWait();
        if (m_StoreController == null)
        {
            // Begin to configure our connection to Purchasing
            InitializePurchasing();
        }
    }

	public void ShowWait () {
		if (_pleaseWait != null)
			_pleaseWait.gameObject.SetActive(true);
		K2.Time.Add(HideWait, 10, 1);
	}

	public void HideWait () {
		if (_pleaseWait != null)
			_pleaseWait.gameObject.SetActive(false);
		K2.Time.Remove(HideWait);
	}

    public void InitializePurchasing()
    {
        // If we have already connected to Purchasing ...
        if (IsInitialized())
        {
            // ... we are done here.
            return;
        }

        // Create a builder, first passing in a suite of Unity provided stores.
        var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

        // Add a product to sell / restore by way of its identifier, associating the general identifier
        // with its store-specific identifiers.
		AddProduct(builder, COINS_250);
		AddProduct(builder, COINS_690);
		AddProduct(builder, COINS_1500);
		AddProduct(builder, COINS_3250);
		AddProduct(builder, COINS_8750);
		AddProduct(builder, OFFER_1_1);
		AddProduct(builder, OFFER_1_2);
		AddProduct(builder, OFFER_2_1);
		AddProduct(builder, OFFER_2_2);
		AddProduct(builder, OFFER_3_1);
		AddProduct(builder, OFFER_4_1);
        // Kick off the remainder of the set-up with an asynchrounous call, passing the configuration 
        // and this class' instance. Expect a response either in OnInitialized or OnInitializeFailed.
        UnityPurchasing.Initialize(this, builder);
    }

	void AddProduct (ConfigurationBuilder builder, string id) {
		print ("Add "+ id);
		builder.AddProduct(id, ProductType.Consumable , new IDs {
			{id, ProductType.NonConsumable, GooglePlay.Name},
			{id, ProductType.NonConsumable, AppleAppStore.Name}});
	}


    private bool IsInitialized()
    {
        // Only say we are initialized if both the Purchasing references are set.
        return m_StoreController != null && m_StoreExtensionProvider != null;
    }


    public void BuyCoins50()
    {
        // Buy the consumable product using its general identifier. Expect a response either 
        // through ProcessPurchase or OnPurchaseFailed asynchronously.
       // BuyProductID(COINS_50);
    }


    public void BuyCoinPK1()
    {
        BuyProductID(COINS_125);
    }
    public void BuyCoinPK2()
    {
        BuyProductID(COINS_250);
    }
    public void BuyCoinPK3()
    {
        BuyProductID(COINS_690);
    }
    public void BuyCoinPK4()
    {
        BuyProductID(COINS_1500);
    }
    public void BuyCoinPK5()
    {
        BuyProductID(COINS_3250);
    }
    public void BuyCoinPK6()
    {
        BuyProductID(COINS_8750);
    }

    public void BuySpecial1()
    {
        BuyProductID(OFFER_1_1);
    }

    public void BuySpecial2()
    {
        BuyProductID(OFFER_1_2);
    }

    public void BuySpecial3()
    {
        BuyProductID(OFFER_2_1);
    }

    public void BuySpecial4()
    {
        BuyProductID(OFFER_2_2);

    }

    public void BuySpecial5()
    {
        BuyProductID(OFFER_3_1);
    }

    public void BuySpecial6()
    {
        BuyProductID(OFFER_4_1);
    }

    void BuyProductID(string productId)
    {
		ShowWait();
        // If Purchasing has been initialized ...
        if (IsInitialized())
        {
            // ... look up the Product reference with the general product identifier and the Purchasing 
            // system's products collection.
            Product product = m_StoreController.products.WithID(productId);

            // If the look up found a product for this device's store and that product is ready to be sold ... 
            if (product != null && product.availableToPurchase)
            {
                Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
                // ... buy the product. Expect a response either through ProcessPurchase or OnPurchaseFailed 
                // asynchronously.
                m_StoreController.InitiatePurchase(product);
            }
            // Otherwise ...
            else
            {
                // ... report the product look-up failure situation  
                Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
                _pleaseWait.gameObject.SetActive(false);
                if (OnFail != null) OnFail();
            }
        }
        // Otherwise ...
        else
        {
            // ... report the fact Purchasing has not succeeded initializing yet. Consider waiting longer or 
            // retrying initiailization.
            Debug.Log("BuyProductID FAIL. Not initialized.");
			HideWait();
            if (OnFail != null) OnFail();

        }
    }


    // Restore purchases previously made by this customer. Some platforms automatically restore purchases, like Google. 
    // Apple currently requires explicit purchase restoration for IAP, conditionally displaying a password prompt.
    public void RestorePurchases()
    {
        // If Purchasing has not yet been set up ...
        if (!IsInitialized())
        {
            // ... report the situation and stop restoring. Consider either waiting longer, or retrying initialization.
            Debug.Log("RestorePurchases FAIL. Not initialized.");
            return;
        }

        // If we are running on an Apple device ... 
        if (Application.platform == RuntimePlatform.IPhonePlayer ||
            Application.platform == RuntimePlatform.OSXPlayer)
        {
            // ... begin restoring purchases
            Debug.Log("RestorePurchases started ...");

            // Fetch the Apple store-specific subsystem.
            var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
            // Begin the asynchronous process of restoring purchases. Expect a confirmation response in 
            // the Action<bool> below, and ProcessPurchase if there are previously purchased products to restore.
            apple.RestoreTransactions((result) => {
                // The first phase of restoration. If no more responses are received on ProcessPurchase then 
                // no purchases are available to be restored.
                Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
            });
        }
        // Otherwise ...
        else
        {
            // We are not running on an Apple device. No work is necessary to restore purchases.
            Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
        }
    }


    //  
    // --- IStoreListener
    //

    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        // Purchasing has succeeded initializing. Collect our Purchasing references.
        Debug.Log("OnInitialized: PASS");

        // Overall Purchasing system, configured with products for this application.
        m_StoreController = controller;
        // Store specific subsystem, for accessing device-specific store features.
        m_StoreExtensionProvider = extensions;
    }


    public void OnInitializeFailed(InitializationFailureReason error)
    {
        // Purchasing set-up has not succeeded. Check error for reason. Consider sharing this reason with the user.
        Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
    }


    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
    {
		Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));

		if (String.Equals(args.purchasedProduct.definition.id, COINS_250, StringComparison.Ordinal)) PurchaseMatch(250);
		if (String.Equals(args.purchasedProduct.definition.id, COINS_690, StringComparison.Ordinal)) PurchaseMatch(690);
		if (String.Equals(args.purchasedProduct.definition.id, COINS_1500, StringComparison.Ordinal)) PurchaseMatch(1500);
		if (String.Equals(args.purchasedProduct.definition.id, COINS_3250, StringComparison.Ordinal)) PurchaseMatch(3250);
		if (String.Equals(args.purchasedProduct.definition.id, COINS_8750, StringComparison.Ordinal)) PurchaseMatch(8750);
		if (String.Equals(args.purchasedProduct.definition.id, OFFER_1_1, StringComparison.Ordinal)) PurchaseMatch(1500);
		if (String.Equals(args.purchasedProduct.definition.id, OFFER_1_2, StringComparison.Ordinal)) PurchaseMatch(1500);
		if (String.Equals(args.purchasedProduct.definition.id, OFFER_2_1, StringComparison.Ordinal)) PurchaseMatch(3250);
		if (String.Equals(args.purchasedProduct.definition.id, OFFER_2_2, StringComparison.Ordinal)) PurchaseMatch(3250);
		if (String.Equals(args.purchasedProduct.definition.id, OFFER_3_1, StringComparison.Ordinal)) PurchaseMatch(1500);
		if (String.Equals(args.purchasedProduct.definition.id, OFFER_4_1, StringComparison.Ordinal)) PurchaseMatch(3250);


        // Return a flag indicating whether this product has completely been received, or if the application needs 
        // to be reminded of this purchase at next app launch. Use PurchaseProcessingResult.Pending when still 
        // saving purchased products to the cloud, and when that save is delayed. 
        return PurchaseProcessingResult.Complete;
    }

	public void PurchaseMatch (int gold) {
		// The consumable item has been successfully purchased, add 100 coins to the player's in-game score.
		Profile.Instance.Gold += gold;
		if(_pleaseWait != null)
			_pleaseWait.gameObject.SetActive(false);
		if (OnSuccess != null) OnSuccess();
		else Debug.Log("NULL");
	}


    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        // A product purchase attempt did not succeed. Check failureReason for more detail. Consider sharing 
        // this reason with the user to guide their troubleshooting actions.
        if (_pleaseWait != null)
            _pleaseWait.gameObject.SetActive(false);
        if (OnFail != null) OnFail();
        Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
    }
}