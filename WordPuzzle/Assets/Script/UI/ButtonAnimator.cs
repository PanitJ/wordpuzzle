﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class ButtonAnimator : MonoBehaviour, IPointerDownHandler, IPointerExitHandler, IPointerUpHandler,
    IPointerClickHandler
{
    [SerializeField] private float _downOffset = -10f;
    [SerializeField] private GameObject _animateObject;
    private ButtonState _state = ButtonState.Up;

    private Vector2 _defaultPosition, _downPosition;

    private void Start()
    {
        _defaultPosition = new Vector2(transform.localPosition.x, transform.localPosition.y);
        _downPosition = new Vector2(transform.localPosition.x, transform.localPosition.y + _downOffset);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        TransitionToState(ButtonState.Up, OnPress);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        TransitionToState(ButtonState.Down, OnDown);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        TransitionToState(ButtonState.Up, OnUp);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        TransitionToState(ButtonState.Up, OnUp);
    }

    private void Awake()
    {
        if (_animateObject == null)
            _animateObject = gameObject;
    }

    private void TransitionToState(ButtonState state, Action action)
    {
        if (_state == state)
            return;

        _state = state;
        action();
    }

    private void OnUp()
    {
        _animateObject.transform.localPosition = _defaultPosition;
    }

    private void OnDown()
    {
        _animateObject.transform.localPosition = _downPosition;
    }

    private void OnPress()
    {
    }

    private enum ButtonState
    {
        Down,
        Up
    }
}