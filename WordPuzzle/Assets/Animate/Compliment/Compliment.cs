﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Compliment : MonoBehaviour {
    public Animator anim;
    public SpriteRenderer sRenderer;
    [SerializeField] private Text _text;
    public enum Type { Amazing, Excellent, Fantastic, GoodJob, WellDone };
    private string[] complimentText = {"Amazing!","Excellent!","Fantastic!","Good Job!","Well Done" };
    public Sprite[] sprites;

    public void Show(Type type)
    {
        if (!IsAvailable2Show()) return;
        _text.text = complimentText[(int)type];
    //    sRenderer.sprite = sprites[(int)type];
        anim.SetTrigger("show");
        Debug.Log("Show anim");
    }

    public bool IsAvailable2Show()
    {
        AnimatorStateInfo info = anim.GetCurrentAnimatorStateInfo(0);
        return info.IsName("Idle");
    }
}
