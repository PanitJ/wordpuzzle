﻿using UnityEngine.UI;
using K2.Popup;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SettingsPopup : PopupContent
{
    [SerializeField] private Image _soundImage;
    [SerializeField] private Image _musicImage;
    [SerializeField] private Sprite _soundOn;
    [SerializeField] private Sprite _soundOff;
    [SerializeField] private Sprite _musicOn;
    [SerializeField] private Sprite _musicOff;
    [SerializeField] private HelpPopup _helpPopup;

    public SettingsPopup CloneAndDeploy()
    {
        var clone = Instantiate(this);
        clone.Deploy();
        return clone;
    }

    public void Deploy()
    {
        Debug.Log("DeployHelpPopup");
        _soundImage.sprite = SoundManager.instance.IsSoundEnabled() ? _soundOn : _soundOff;
        _musicImage.sprite = SoundManager.instance.IsMusicEnabled() ? _musicOn : _musicOff;
    }

    public void ToggleSound()
    {
        // TODO: This should be able to use a single toggle.
        SoundManager.instance.PlayButtonSound();
        var enable = !SoundManager.instance.IsSoundEnabled();
        SoundManager.instance.SetSoundEnabled(enable);
        _soundImage.sprite = enable ? _soundOn : _soundOff;
        SoundManager.instance.UpdateSetting();
        FireBaseManager.instance.LogSoundToogle();

    }

    public void ToogleMusic()
    {
        SoundManager.instance.PlayButtonSound();
        var enable = !SoundManager.instance.IsMusicEnabled();
        SoundManager.instance.SetMusicEnabled(enable);
        _musicImage.sprite = enable ? _musicOn : _musicOff;
        SoundManager.instance.UpdateSetting();
        FireBaseManager.instance.LogMusicToogle();
    }

    public void Help()
    {
        SoundManager.instance.PlayButtonSound();
        var popup = _helpPopup.CloneAndDeploy();
        PopupSystem.Instance.Show();
        PopupSystem.Instance.Add(popup);
    }
    public void Close()
    {
        SoundManager.instance.PlayButtonSound();
        PopupSystem.Instance.CloseLayer();
    }
}
