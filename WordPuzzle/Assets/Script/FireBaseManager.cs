﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBaseManager : MonoBehaviour {

    public static FireBaseManager instance = null;

    void Awake()
    {
        //Check if there is already an instance of SoundManager
        if (instance == null)
            //if not, set it to this.
            instance = this;
        //If instance already exists:
        else if (instance != this)
            //Destroy this, this enforces our singleton pattern so there can only be one instance of SoundManager.
            Destroy(gameObject);

        //Set SoundManager to DontDestroyOnLoad so that it won't be destroyed when reloading our scene.
        DontDestroyOnLoad(gameObject);
    }

    public void LogLangaugeToogle()
    {
      //  Firebase.Analytics.FirebaseAnalytics.LogEvent("langauge_change_event", "percent", 0.4f);
    }

    public void LogPlay()
    {
     //   Firebase.Analytics.FirebaseAnalytics.LogEvent("play_press_event", "percent", 0.4f);
    }

    public void LogSoundToogle()
    {
     //   Firebase.Analytics.FirebaseAnalytics.LogEvent("sound_toogle_event", "percent", 0.4f);
    }

    public void LogMusicToogle()
    {
    //    Firebase.Analytics.FirebaseAnalytics.LogEvent("music_toogle_event", "percent", 0.4f);
    }

    public void LogModeSelect(int i)
    {
  //      Firebase.Analytics.FirebaseAnalytics.LogEvent("mode_select_event", "value", i);
    }

    public void LogLevel(int i)
    {
     //   Firebase.Analytics.FirebaseAnalytics.LogEvent("level_event", "value", i);
    }

    public void LogHint(int idVariant, int mainlevel,int sublevel)
    {
    //    Firebase.Analytics.FirebaseAnalytics.LogEvent("hint_event", "percent", 0.4f);
    }

    public void LogBack()
    {
     //   Firebase.Analytics.FirebaseAnalytics.LogEvent("back_event", "percent", 0.4f);
    }

    public void LogIAPPopup()
    {
    //    Firebase.Analytics.FirebaseAnalytics.LogEvent("IAP_event", "percent", 0.4f);
    }

	public void LogMaxLevel(int idVariant, int maxLevel)
	{
		Firebase.Analytics.FirebaseAnalytics.LogEvent(idVariant + "max_level", "value", maxLevel);
	}

    public void LogExit (int idVariant, int maxLevel)
    {
        Firebase.Analytics.FirebaseAnalytics.LogEvent(idVariant + "max_level to exit", "value", maxLevel);
    }

}
