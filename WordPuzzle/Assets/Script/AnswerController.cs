﻿using System.Collections.Generic;
using K2.Word;
using UnityEngine;
using System;

public class AnswerController : MonoBehaviour
{
    private const float originGapX = 100f;
    private WordChecklist _wordChecklist;
    [SerializeField] private WordView _wordViewPrefab;
    private List<WordView> answerWords = new List<WordView>();

   // private const float boardHigh = 400f;
 //   private const float boardWidth = 640f;
    private const float boardWidth = 625f;
    private const float boardHigh = 430f;
    private const float offsetX = 12f;

    private Vector3 beginPosition;

    private float gapX, gapY;
    private float scale = 1.0f;
    private int _hintWordIndex = 0;
    private int _hintLetterIndex = 0;
    float _letterSize;

    public event Action OnExtraWordComplete;
    public event Action OnWordComplete;

    int _starIndex = -1;
    public bool hasBonus = false;

    public void Calibrate(string[] alphabetStrings, int alphabetsNumber, string[] extrawordStrings,int starIndex = -1)
    {
        _starIndex = starIndex;
        beginPosition = new Vector3(-boardWidth / 2f, boardHigh / 2f, 0);
        InitBoardAnswer(alphabetStrings, alphabetsNumber, _starIndex);
        InitExtraWordStuff(extrawordStrings);
        _hintWordIndex = 0;
        _hintLetterIndex = 0;
    }

    private WordChecklist _extrawordChecklist;
    private void InitExtraWordStuff(string[] extrawordStrings)
    {
		if (extrawordStrings == null || extrawordStrings.Length <= 0) return;
        _extrawordChecklist = new WordChecklist(extrawordStrings);
    }

    public bool CheckExtraAnswer(string selectedString)
    {
		if (_extrawordChecklist == null) return false;

        if (Profile.Instance.HavePlayedExtrawords.Contains(selectedString)) return false;
        int indexOfAnswer = _extrawordChecklist.IndexOfWordValid(selectedString);
        Debug.Log("Check Extra " + selectedString + indexOfAnswer);
        if (indexOfAnswer > -1)
        {
            Profile.Instance.HavePlayedExtrawords.Add(selectedString);
            if (OnExtraWordComplete != null) OnExtraWordComplete();
            return true;
        }
        return false;
    }

    private void InitBoardAnswer(string[] alphabetStrings, int alphabetsNumber, int starIndex)
    {
        _wordChecklist = new WordChecklist(alphabetStrings);
        SetScale(alphabetsNumber);
        var halfWordCheckListSize = Mathf.FloorToInt(_wordChecklist.WordTargets.Count / 2f + 0.5f);

        for (var i = 0; i < _wordChecklist.WordTargets.Count; i++)
        {
            var row = i % halfWordCheckListSize; 
            var column = i / halfWordCheckListSize;
            var pos = WordPosition(column, row);
            var newWord = Instantiate<WordView>(_wordViewPrefab, transform);
            bool isStar = false;
            if (i == starIndex) isStar = true;
            newWord.Calibate(_wordChecklist.WordTargets[i].Word, pos, scale, isStar);
            answerWords.Add(newWord);
        }
    }

    private void SetScale(int alphabetsNumber)
    {
        var defaultScale = 3.0f;
        scale = defaultScale / alphabetsNumber;
        SetGap(alphabetsNumber);
    }

    private void SetGap(int alphabetsNumber)
    {
        int rowNumber = Mathf.FloorToInt(_wordChecklist.WordTargets.Count / 2.0f + 0.5f);
        _letterSize = originGapX * scale;
        gapX = _letterSize * alphabetsNumber;
        float spaceYInBoard = boardHigh - (rowNumber * _letterSize);
        float gapNumber = rowNumber + 1f;
        gapY = spaceYInBoard / gapNumber;
    }



    private Vector3 WordPosition(int column, int row)
    {
        float letterRadius = _letterSize / 2f;
        float beginX = beginPosition.x + letterRadius + letterRadius/2f + (offsetX/scale); /// beginpos at left bound  - gap for X(letterraidus/2)
        float beginY = beginPosition.y - letterRadius - gapY;
        float gapBetweenLetterFromCenter = -gapY - _letterSize;
        return new Vector3(beginX, beginY) + new Vector3(column * gapX, row * gapBetweenLetterFromCenter, 0);
    }

    public bool CheckAnswer(string selectedString)
    {
        int indexOfAnswer = _wordChecklist.IndexOfWordValid(selectedString);
        if (indexOfAnswer > -1)
        {
            UpdateBoard(indexOfAnswer);
            return true;
        }
        return false;
    }

    public bool IsLevelComplete()
    {
        return _wordChecklist.IsComplete;
    }

    public void UpdateBoard(int index)
    {
        if (OnWordComplete != null) OnWordComplete();
        answerWords[index].RevealWord();
        if (index == _starIndex)
            hasBonus = true;
    }

    public void Hint()
    {
        Profile.Instance.Gold -= Profile.HINT_COST;

        while (_hintLetterIndex >= answerWords[_hintWordIndex].Count || answerWords[_hintWordIndex].IsComplete)
        {
            _hintWordIndex++;
            if (_hintWordIndex >= answerWords.Count)
            {
                _hintWordIndex = 0;
                _hintLetterIndex++;
            }
        }
     
        Debug.Log("Hint " + _hintWordIndex + "   " + _hintLetterIndex);
        answerWords[_hintWordIndex].Hint(_hintLetterIndex);
        if (_hintLetterIndex == answerWords[_hintWordIndex].Count - 1)
            _wordChecklist.WordTargets[_hintWordIndex].Complete();

        _hintWordIndex++;
        if (_hintWordIndex >= answerWords.Count)
        {
            _hintWordIndex = 0;
            _hintLetterIndex++;
        }

    }
}