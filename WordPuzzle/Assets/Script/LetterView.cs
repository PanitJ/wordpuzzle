﻿using System;
using UnityEngine;
using  UnityEngine.UI;

public class LetterView : MonoBehaviour
{
    private char value;
   // [SerializeField] private Text label;
    [SerializeField] private Image _letterImage;
    [SerializeField] private Image _star;

    public event Action<float> OnReveal;
    const float reveal_Time = 0.2f;

    public LetterView()
    {
        IsRevealed = false;
        OnReveal += Animate;
    }

    public bool IsRevealed { get; set; }

    public void Calibrate(char value,bool star)
    {
        this.value = value;
        IsRevealed = false;
        _letterImage.sprite = Resources.Load<Sprite>("Font/" + value);
        _letterImage.gameObject.SetActive(false);
        _star.gameObject.SetActive(star);

    }

    public void Reveal(float delay = 0f)
    {
        if (_star.gameObject.activeInHierarchy) _star.gameObject.SetActive(false);
        if (OnReveal != null) OnReveal(delay);
        IsRevealed = true;
        _letterImage.gameObject.SetActive(true);
    }

    void Animate(float delay)
    {
        LeanTween.scale(this.gameObject, new Vector3(1.5f, 1.5f, 1.0f), reveal_Time).setDelay(delay);
        LeanTween.scale(this.gameObject, new Vector3(1.0f, 1.0f, 1.0f), reveal_Time).setDelay(delay + reveal_Time);
    }

    public static float AnimationTime
    {
        get {
            return reveal_Time * 3f;
        }
    }
}
