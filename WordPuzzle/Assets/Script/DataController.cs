﻿using System;
using UnityEngine;

public class DataController
{
    private const string MAX_MODE_KEY = "MAX_MODE_KEY";
    private const string MAX_MAIN_LEVEL_KEY = "MAX_MAIN_LEVEL_KEY";
    private const string MAX_SUB_LEVEL_KEY = "MAX_SUB_LEVEL_KEY";
    private const string CURRENT_MODE_KEY = "CURRENT_MODE_KEY";
    private const string CURRENT_MAIN_LEVEL_KEY = "CURRENT_MAIN_LEVEL_KEY";
    private const string CURRENT_SUB_LEVEL_KEY = "CURRENT_SUB_LEVEL_KEY";

    private const string GOLD = "GOLD";
    private const string CURRENT_PLAYED_WORD = "CURRENT_PLAYED_WORD";
    private const string CURRENT_CSV = "CURRENT_CSV";



    public DataController()
    {
        Load();
    }

    public void Save()
    {
        Debug.Log("save max " + LevelManager.instance.MaxProgress);
        PlayerPrefs.SetInt(MAX_MAIN_LEVEL_KEY, LevelManager.instance.MaxProgress);
        PlayerPrefs.SetInt(MAX_SUB_LEVEL_KEY, LevelManager.instance.MaxSubProgress);
        PlayerPrefs.SetInt(CURRENT_MAIN_LEVEL_KEY, Profile.Instance.CurrentLevelData.mainLevel);
        PlayerPrefs.SetInt(CURRENT_SUB_LEVEL_KEY, Profile.Instance.CurrentLevelData.currentSubLevel);
        PlayerPrefs.SetInt(GOLD, Profile.Instance.Gold);

        if (Profile.Instance.CurrentPlayedWords != null && Profile.Instance.CurrentPlayedWords.Count > 0 && Profile.Instance.CurrentPlayedWords[0].Length > 0)
        {
            var playedWordsString = string.Empty;
            for (var i = 0; i < Profile.Instance.CurrentPlayedWords.Count; i++)
            {
                playedWordsString = string.Concat(playedWordsString, Profile.Instance.CurrentPlayedWords[i]);
                if(i < Profile.Instance.CurrentPlayedWords.Count - 1) //Check is not last word
                    playedWordsString = string.Concat(playedWordsString, "*");
            }
            PlayerPrefs.SetString(CURRENT_PLAYED_WORD, playedWordsString);
            PlayerPrefs.SetString(CURRENT_CSV,Profile.Instance.CurrentTextAssetName);
        }
      
        PlayerPrefs.Save();
    }

    public void Load()
    {
        Profile.Instance.MaxMainLevel = PlayerPrefs.GetInt(MAX_MAIN_LEVEL_KEY, 0);
        Profile.Instance.MaxSubLevel = PlayerPrefs.GetInt(MAX_SUB_LEVEL_KEY, 0);
        Profile.Instance.CurrentMainLevel = PlayerPrefs.GetInt(CURRENT_MAIN_LEVEL_KEY, 0);
        Profile.Instance.CurrentSubLevel = PlayerPrefs.GetInt(CURRENT_SUB_LEVEL_KEY, 0);
        Profile.Instance.Gold = PlayerPrefs.GetInt(GOLD, 0);
        var playedWordsString = PlayerPrefs.GetString(CURRENT_PLAYED_WORD, string.Empty);
        if (!playedWordsString.Equals(String.Empty))
        {
            string[] currentStrings= playedWordsString.Split('*');
            for (int i = 0; i < currentStrings.Length; i++)
            {
                Profile.Instance.CurrentPlayedWords.Add(currentStrings[i]);
            }
            Profile.Instance.CurrentTextAssetName = PlayerPrefs.GetString(CURRENT_CSV, string.Empty);
            Profile.Instance.hasLoadData = true;
        }
    }
}
