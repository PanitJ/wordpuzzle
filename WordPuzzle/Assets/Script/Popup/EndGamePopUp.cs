﻿using  UnityEngine.UI;
using K2.Popup;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndGamePopUp : PopupContent
{
    [SerializeField] Sprite[] _modes;
    [SerializeField] Image _mode;
    [SerializeField] Text _level;

    private string _nextScene = "Undefined";

    public EndGamePopUp CloneAndDeploy(string nextScene, int gold, int mode, int level)
    {
        var clone = Instantiate(this);
        clone.Deploy(nextScene, gold,mode,level);
        return clone;
    }

    public void Deploy(string nextScene, int gold,int mode, int level)
    {
        _mode.sprite = _modes[mode];
        _level.text = LevelManager.instance.GetMainLevel(mode).name  +"  Level " + (level + 1);
        _nextScene = nextScene;
        Debug.Log("");
    }

    public void Continue()
    {
        Profile.Instance.Gold += Profile.GOLD_PER_LEVEL;
        NAdManager.Instance.ShowAds();
        SceneManager.LoadScene(_nextScene);
    }
}
