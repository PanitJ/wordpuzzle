﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace K2.Popup
{
    public class PopupLayer : MonoBehaviour
    {
        [SerializeField] private Transform _contentHolder;
        [SerializeField] private CanvasGroup _darkener;
        [SerializeField] private Animator _darkenerAnimator, _contentAnimator;

        private readonly Stack<PopupContent> _contents;

        public PopupLayer()
        {
            _contents = new Stack<PopupContent>();
        }

        public bool IsEmpty
        {
            get { return _contents.Count == 0; }
        }

        public void Show()
        {
            _contentHolder.gameObject.SetActive(true);
            
        }

        public void Hide()
        {
            AnimateContentOut();
            _darkenerAnimator.Play("Fade Out");
            var time = _darkenerAnimator.GetCurrentAnimatorStateInfo(0).length;
            StartCoroutine(HideAfterSeconds(time));
        }

        private IEnumerator HideAfterSeconds(float time)
        {
            yield return new WaitForSeconds(time);
            OnHideComplete();
        }

        public void OnHideComplete()
        {
            Destroy();
        }

        public void Destroy()
        {
            Destroy(gameObject);
        }

        public void Display(PopupContent content)
        {
            HideCurrentContent();
            AddContent(content);
        }

        public void Back()
        {
            RemoveContent();
        }

        private void AnimateContentOut()
        {
            _contentAnimator.Play("Hide");
        }

        private void AnimateContentIn()
        {
            _contentAnimator.Play("Show", 0, 0);
        }

        private void HideCurrentContent()
        {
            if (IsEmpty) return;
            var content = _contents.Peek();
            content.gameObject.SetActive(false);
        }

        private void ShowCurrentContent()
        {
            if (IsEmpty) return;
            var content = _contents.Peek();
            content.gameObject.SetActive(true);
            _contentAnimator.Play("Show", 0, 0);
            AnimateContentIn();
        }

        private void AddContent(PopupContent content)
        {
            _contents.Push(content);
            content.transform.SetParent(_contentHolder);
            content.transform.localPosition = Vector3.zero;
            content.transform.localScale = Vector3.one;
            AnimateContentIn();
        }

        private void RemoveContent()
        {
            if (IsEmpty) return;
            var content = _contents.Pop();
            if (!IsEmpty)
                Destroy(content.gameObject);
            ShowCurrentContent();
        }
    }
}