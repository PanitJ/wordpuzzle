﻿using System;
using System.Collections.Generic;
using UnityEngine;
using K2.Popup;

public class LevelManager : MonoBehaviour {

    [SerializeField] Sprite[] iconsNormal;
    [SerializeField] Sprite[] iconsActive;
    [SerializeField] Sprite[] titles;
    [SerializeField] TextAsset[] csvs;
    [SerializeField] TextAsset[] csvs_v2;
    [SerializeField] TextAsset[] csvs_v3;
    [SerializeField] IAPPopup IAPPopup;

    public enum Theme { Aquatic = 0, Bird = 1, Dinosaur = 2, Savannah = 3 }

    public List<LevelData> allLevels = new List<LevelData>();
    private int _maxProgress;
    public static LevelManager instance = null;
    private int[] themeCounts;
    [SerializeField] private NTimer _nTimer;

    public bool IsStarReady = false;
    private const int TIME_TO_RESTAR = 60;
    const int minuteToOfferEnd = 30;

    private int _levelPlayCount = 0;

    private void Awake()
    {
        //Check if there is already an instance of SoundManager
        if (instance == null)
        {
            //if not, set it to this.
            instance = this;
            _levelPlayCount = 0;
        }
        //If instance already exists:
        else if (instance != this)
            //Destroy this, this enforces our singleton pattern so there can only be one instance of SoundManager.
            Destroy(gameObject);

        //Set SoundManager to DontDestroyOnLoad so that it won't be destroyed when reloading our scene.
        DontDestroyOnLoad(gameObject);
    }

    public void Load()
    {
        CreateLevel();
    }

    public void UnlockAllLevel()
    {
        SetMaxProgress(allLevels.Count, 1);
    }

    private void CreateLevel()
    {
        switch (Profile.Instance.Variant)
        {
            case 1:
                CreateLevelByVariant(csvs);
                break;
            case 2:
                CreateLevelByVariant(csvs_v2);
                break;
            case 3:
                CreateLevelByVariant(csvs_v3);
                break;
        }
        //themeCounts = new int[iconsNormal.Length];
        //for (var i = 0; i < csvs.Length; i++)
        //{
        //    var newLevelData = new LevelData();
        //    newLevelData.Init(csvs[i]);
        //    var num = newLevelData.themeNumber;
        //    newLevelData.Calibrate(iconsNormal[num], iconsActive[num], titles[num], i, false, 0);
        //    allLevels.Add(newLevelData);
        //    themeCounts[num]++;
        //}
    }

    private void CreateLevelByVariant(TextAsset[] csv)
    {
        themeCounts = new int[iconsNormal.Length];
        for (var i = 0; i < csvs.Length; i++)
        {
            var newLevelData = new LevelData();
            if (i < csv.Length)
                newLevelData.Init(csv[i]);
            else
                newLevelData.Init(csvs[i]);

            var num = newLevelData.ThemeNumber;
            newLevelData.Calibrate(iconsNormal[num], iconsActive[num], titles[num], i, false, 0);
            allLevels.Add(newLevelData);
            themeCounts[num]++;
        }
    }

    public void SetMaxProgress(int maxMode, int maxLevel)
    {
        for (var i = 0; i < allLevels.Count; i++)
        {
            if (i < maxMode)
                allLevels[i].Complete();
            else if(i == maxMode)
            {
                allLevels[i].MaxLevel = maxLevel;
                _maxProgress = i;
            }
        }
    }

    public LevelData GetMainLevel(int index)
    {
        return allLevels[index];
    }

    public char[] GetSubLevel(int index)
    {
        return allLevels[Profile.Instance.CurrentMainLevel].LetterListOfIndex(index);
      //  return Profile.Instance.CurrentLevelData.LetterListOfIndex(index);

    }

    public int MaxProgress {
        get
        {
            if (_maxProgress > allLevels.Count - 1)
                _maxProgress = allLevels.Count - 1;
            return _maxProgress;
        }
    }

    public int MaxSubProgress {
        get
        {
            return allLevels[MaxProgress].MaxLevel;
        }
    }

    public int ThemeCount {
        get
        {
            return iconsNormal.Length;
        }
    }

    public int CountForTheme(int i)
    {
        return themeCounts[i];
    }

    private void UnlockNextMainLevel(int index)
    {
        index++;
        //if (!allLevels[index].HasClear)
        //    allLevels[index].MaxLevel = 0;
        Profile.Instance.CurrentLevelData = allLevels[index];
    }

    public void Progress()
    {
        if (Profile.Instance.CurrentLevelData.mainLevel < MaxProgress || Profile.Instance.CurrentLevelData.HasClear) return;

        _levelPlayCount++;
        Profile.Instance.CurrentLevelData.Progress();  //Sub level Progress
        if (MaxProgress == 0 && Profile.Instance.CurrentLevelData.mainLevel == 5) ShowOfferFirstTime();

        if(((Profile.Instance.CurrentLevelData.currentSubLevel % 15) == 0)
            || (Profile.Instance.Gold <= 25 && _levelPlayCount > 5) 
            || (Profile.Instance.CurrentLevelData.currentSubLevel % 7 == 0 && Profile.Instance.Gold < 50 && _levelPlayCount >= 5))
        {
            ShowOffer();
            _levelPlayCount = 0;
        }

       // if( vel % 7 == 0 && gold < 50 && count >= 5)
        if (Profile.Instance.CurrentLevelData.HasClear) //Main level progress
        {
            UnlockNextMainLevel(Profile.Instance.CurrentLevelData.mainLevel);
            _maxProgress++;
            FireBaseManager.instance.LogMaxLevel(Profile.Instance.Variant, _maxProgress);
        }

        Profile.Instance.CurrentPlayedWords.Clear();
    }

    public void StartTimerForStar()
    {
        IsStarReady = false;
        _nTimer.StartTimer(TIME_TO_RESTAR, OnStarReady);
    }

    public void OnStarReady()
    {
        IsStarReady = true;
    }

    public void StartTimerForOffer()
    {
        Debug.Log("Start time for offer");
        _nTimer.StartTimer(minuteToOfferEnd, OfferEnd);
    }

    void OfferEnd()
    {
        Profile.Instance.OfferNumber = 0;
    }

    public void ShowOfferFirstTime()
    {
        if (!Profile.Instance.CanOffer) return;
        Profile.Instance.CanOffer = false;
        Profile.Instance.OfferNumber = K2.Dice.Range(1, 7);
        SoundManager.instance.PlayButtonSound();
        IAPPopup.IAPType type = (IAPPopup.IAPType)Profile.Instance.OfferNumber;
        PopupSystem.Instance.Show();
        var content = Instantiate(IAPPopup);
        content.HeaderBarController = FindObjectOfType<HeaderBarController>();
        content.Deploy(type);
        PopupSystem.Instance.Add(content);
        StartTimerForOffer();
    }

    public void ShowOffer ()
    {
        Profile.Instance.OfferNumber = K2.Dice.Range(1, 7);
        SoundManager.instance.PlayButtonSound();
        IAPPopup.IAPType type = (IAPPopup.IAPType)Profile.Instance.OfferNumber;
        PopupSystem.Instance.Show();
        var content = Instantiate(IAPPopup);
        content.HeaderBarController = FindObjectOfType<HeaderBarController>();
        content.Deploy(type);
        PopupSystem.Instance.Add(content);
        StartTimerForOffer();
    }
}
