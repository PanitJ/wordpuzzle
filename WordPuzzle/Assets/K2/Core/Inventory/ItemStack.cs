﻿using System;

namespace TapSlots.Inventory
{
    public interface IItemStack<T>
    {
        T Item { get; }
        int Count { set; get; }
        bool IsEqualToItem(T item);
        bool IsEqualToItem(IItemStack<T> itemStack);
        bool CanStackWith(IItemStack<T> itemStack);
    }

    public class ItemStack<T> : IItemStack<T>
    {
        private int m_count;
        private T m_item;

        public ItemStack(T item, int count = 0)
        {
            m_item = item;
            m_count = count;
        }

        public virtual int Count
        {
            set
            {
                m_count = Math.Max(0, value);
            }
            get
            {
                return m_count;
            }
        }

        public virtual T Item
        {
            get
            {
                return m_item;
            }
        }

        public virtual bool IsEqualToItem(T item)
        {
            return Item.Equals(item);
        }

        public virtual bool IsEqualToItem(IItemStack<T> stack)
        {
            return stack != null && IsEqualToItem(stack.Item);
        }

        public virtual bool CanStackWith(IItemStack<T> stack)
        {
            return stack != null && IsEqualToItem(stack);
        }

        public override string ToString()
        {
            return string.Format("[{0}] {1}", "Stack", string.Format("{0}: {1}", Item, Count));
        }
    }
}