﻿using UnityEngine;
using UnityEngine.UI;

public class LetterPreview : MonoBehaviour {
    [SerializeField] Text letters;

    public void Calibrate (char c,int fontSize = 38)
    {
        letters.text = "" + char.ToUpper(c);
        letters.fontSize = fontSize;
    }
}
